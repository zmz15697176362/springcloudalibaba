package com.zmz.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2023/03/19/18:20
 * @Description:
 */
@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {
    @Value("${user.name}")
    public String userName;

    @GetMapping("/show")
    public String show() {
        System.out.println("userName:" + userName);
        return userName;
    }
}
