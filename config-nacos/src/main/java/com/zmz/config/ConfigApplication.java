package com.zmz.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2023/03/19/16:36
 * @Description: 配置中心服务启动类
 */
@SpringBootApplication
public class ConfigApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ConfigApplication.class, args);
        String orderName = applicationContext.getEnvironment().getProperty("order.name");
        String num = applicationContext.getEnvironment().getProperty("order.num");
        String userConfig = applicationContext.getEnvironment().getProperty("user.config");
        System.out.println("order name :" + orderName + "; num: " + num);
        System.out.println("userConfig :" + userConfig);
      
    }
}
