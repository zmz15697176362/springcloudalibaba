package com.zmz.order.controller;

import com.zmz.order.Domain.TestA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2022/08/09/20:49
 * @Description: 订单服务
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    TestA testA;

    @RequestMapping("/add")
    public String add() {
        System.out.println("下单成功");
        TestA test2 = new TestA();
        System.out.println(test2.getPort());
        System.out.println(testA.getPort());
        test2.show();
        testA.show();
        String msg = restTemplate.getForObject("http://localhost:8011/stock/reduct", String.class);
        return "Hello word!" + msg;
    }
}
