package com.zmz.order.Domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2022/09/14/11:07
 * @Description:
 */

@Component
public class TestA {
    @Value("${server.port}")
    private String port;


    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public void show() {
        System.out.println(port);
    }
}
