package com.zmz.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2022/08/09/20:49
 * @Description: 订单服务
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/add")
    public String add() {
        System.out.println("下单成功");
        //在RestTemplate配置的时候加上了@LoadBalance启动负载均衡后就必须使用服务名进行调用了 如果使用ip端口调用则会报错
//        String msg = restTemplate.getForObject("http://192.168.13.1:8021/stock/reduct",String.class);
        String msg = restTemplate.getForObject("http://stock-service/stock/reduct", String.class);
        return "Hello word!" + msg;
    }
}
