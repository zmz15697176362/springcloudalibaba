package com.zmz.ribbon;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2023/03/13/12:35
 * @Description:
 */
@Configuration
public class RibbonRandomRuleConfig {
    @Bean
    public IRule iRule() {
        return new RandomRule();
    }
}
