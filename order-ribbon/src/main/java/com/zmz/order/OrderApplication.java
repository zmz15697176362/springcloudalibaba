package com.zmz.order;

import com.zmz.ribbon.CustomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2022/08/09/21:05
 * @Description:
 */
@SpringBootApplication
@EnableDiscoveryClient   //启动nacos的客户端 不加也行在后续的版本这个注解可不用手动添加
@RibbonClients(value = {
        // 在SpringBoot主程序扫描的包外定义配置类
//        @RibbonClient(name = "stock-service", configuration = RibbonRandomRuleConfig.class)
        @RibbonClient(name = "stock-service", configuration = CustomRule.class)
})
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    @Bean
    @LoadBalanced   //负载均衡注解，加上后就必须使用服务名调用
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        RestTemplate restTemplate = builder.build();
        return restTemplate;
    }


}
