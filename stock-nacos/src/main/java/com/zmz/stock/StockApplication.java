package com.zmz.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2022/08/09/21:08
 * @Description:  库存启动类
 */
@SpringBootApplication
@EnableDiscoveryClient   //启动nacos的客户端 不加也行在后续的版本这个注解可不用手动添加
public class StockApplication {
    public static void main(String[] args) {
        SpringApplication.run(StockApplication.class,args);
    }
}
