package com.zmz.stock.controller;

import com.zmz.order.Domain.TestA;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2022/08/09/20:54
 * @Description:  库存服务
 */
@RestController
@RequestMapping("/stock")
public class StockController {
    TestA a =new TestA();
    @RequestMapping("/reduct")
    public String reduct(){
        System.out.println("扣减库存！");
        return "扣减库存！";
    }
}
