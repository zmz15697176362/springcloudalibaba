# SpringCloudAlibaba微服务调用组件-Feign

> 本项目代码与笔记已存放在Gitee仓库   地址： [代码，笔记](https://gitee.com/zmz15697176362/springcloudalibaba)

[TOC]

**JAVA 项目中如何实现接口调用？**

**1）Httpclient** 

HttpClient 是 Apache Jakarta Common 下的子项目，用来提供高效的、最新的、功能丰富的支持 Http 协议的客户端编程工具包，并且它支持 HTTP 协议最新版本和建议。HttpClient  相比传统 JDK 自带的 URLConnection，提升了易用性和灵活性，使客户端发送 HTTP 请求变 得容易，提高了开发的效率。 

**2）Okhttp** 

一个处理网络请求的开源项目，是安卓端最火的轻量级框架，由 Square 公司贡献，用于替代 HttpUrlConnection 和 Apache HttpClient。OkHttp 拥有简洁的 API、高效的性能，并支持多种协议（HTTP/2 和 SPDY）。 

**3）HttpURLConnection** 

HttpURLConnection 是 Java 的标准类，它继承自 URLConnection，可用于向指定网站发送GET 请求、POST 请求。HttpURLConnection 使用比较复杂，不像 HttpClient 那样容易使用。 

**4）RestTemplate**   **WebClient** 

RestTemplate 是 Spring 提供的用于访问 Rest 服务的客户端，RestTemplate 提供了多种便捷访问远程 HTTP 服务的方法，能够大大提高客户端的编写效率。上面介绍的是最常见的几种调用接口的方法，我们下面要介绍的方法比上面的更简单、方便，它就是 Feign。

## 1. 什么是Feign 

Feign是Netflix开发的声明式、模板化的HTTP客户端**（feign是声明在服务消费端的）**，其灵感来自Retrofit、JAXRS-2.0以及 WebSocket。Feign可帮助我们更加便捷、优雅地调用HTTP API。Feign支持多种注解，例如Feign自带的注解或者JAX-RS注解等。

**Spring Cloud openfeign对Feign进行了增强，使其支持Spring MVC注解，另外还整合了 Ribbon和Nacos，从而使得Feign的使用更加方便 。**

### 1.1 优势 

Feign可以做到使用 HTTP 请求远程服务时就像调用本地方法一样的体验，开发者完全感知不到这是远程方法，更感知不到这是个 HTTP 请求。它像 Dubbo 一样，consumer 直接调用接口方法调用 provider，而不需要通过常规的 Http Client 构造请求再解析返回数据。它解决了让开发者调用远程接口就跟调用本地方法一样，无需关注与远程的交互细节，更无需关注分布式环境开发。

## 2. Spring Cloud Alibaba快速整合OpenFeign 

### 1）引入依赖 

```java
<!--openfeign远程调用-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

### 2）编写调用接口+@FeignClient注解

```java
/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2023/03/15/21:15
 * @Description: 调用stock-service服务service
 */
/*name 指定调用的rest接口的服务名，比如这里调用库存服务stock-service
 * path 指定rest接口所在的StockController指定的@RequestMapping("/stock")
 *  **/
@FeignClient(name = "stock-service", path = "/stock")
public interface StockFeignService {
    //声明需要调用的rest接口对应的方法，直接复制过来，对应的就行(不用写实现类)
    @RequestMapping("/reduct")
    String reduct();
}
```

### 3）调用端在启动类上添加@EnableFeignClients注解(不加的话会找不到客户端service)

```java
@SpringBootApplication
@EnableDiscoveryClient   //启动nacos的客户端 不加也行在后续的版本这个注解可不用手动添加
@EnableFeignClients
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
}
```

### 4）发起调用，像调用本地方式一样调用远程服务

```java
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    StockFeignService stockService;

    @RequestMapping("/add")
    public String add() {
        System.out.println("下单成功");
        String msg = stockService.reduct();
        return "Hello Feign!" + msg;
    }
}
```

## 3. Spring Cloud Feign的自定义配置及使用 

Feign 提供了很多的扩展机制，让用户可以更加灵活的使用。 

### 3.1 日志配置 

有时候我们遇到 Bug，比如接口调用失败、参数没收到等问题，或者想看看调用性能，就需要 

配置 Feign 的日志了，以此让 Feign 把请求信息输出来。 

**1）定义一个配置类，指定日志级别** 

```java
/*  全局配置：当使用@Configuation会将配置作用于所有的服务提供方
 *   局部配置: 如果只想针对某一个服务进行配置，就不要加@Configuration
 */
@Configuration
public class FeignConfig {
    /**
     * 日志级别
     * <p>
     *  @return
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.BASIC;
    }
}
```

通过源码可以看到日志等级有 4 种，分别是： 

**NONE**【性能最佳，适用于生产】：不记录任何日志（默认值）。 

**BASIC**【适用于生产环境追踪问题】：仅记录请求方法、URL、响应状态代码以及 

执行时间。 

**HEADERS**：记录BASIC级别的基础上，记录请求和响应的header。 

**FULL**【比较适用于开发及测试环境定位问题】：记录请求和响应的header、body 

和元数据。 

**2) 局部配置，让调用的微服务生效，在@FeignClient 注解中指定使用的配置类(如果是全局配置就不用加)**

```java
@FeignClient(name = "stock-service", path = "/stock", configuration = FeignConfig.class)
public interface StockFeignService {
    //声明需要调用的rest接口对应的方法，直接复制过来，对应的就行(不用写实现类)
    @RequestMapping("/reduct")
    String reduct();
}
```

**3) 在yml配置文件中执行 Client 的日志级别才能正常输出日志，格式是"logging.level.feign接口包路径** 

**=debug"** （找到对应的service所在的包右键copy reference）

```yaml
logging:
  level:
    com.zmz.order.feign: debug
```

就会显示BASIC级别的日志信息

> 下单成功
> 2023-03-15 22:39:48.867 DEBUG 23504 --- [nio-8040-exec-1] com.zmz.order.feign.StockFeignService    : [StockFeignService#reduct] ---> GET http://stock-service/stock/reduct HTTP/1.1
> 2023-03-15 22:39:48.871 DEBUG 23504 --- [nio-8040-exec-1] com.zmz.order.feign.StockFeignService    : [StockFeignService#reduct] <--- HTTP/1.1 200 (3ms)

**补充：局部配置可以在yml中配置** 

对应属性配置类：  

org.springframework.cloud.openfeign.FeignClientProperties.FeignClientConfiguration

```yaml
#feign日志局部配置
feign:
  client:
    config:
      stock-service:
        loggerLevel: FULL
```

### 3.2 契约配置 （可以把openFeign还原为feign使用feign的原生注解）

Spring Cloud 在 Feign 的基础上做了扩展，使用 Spring MVC 的注解来完成Feign的功 能。原生的 Feign 是不支持 Spring MVC 注解的，如果你想在 Spring Cloud 中使用原生的 注解方式来定义客户端也是可以的，通过配置契约来改变这个配置，Spring Cloud 中默认的 是 SpringMvcContract。 Spring Cloud 1  早期版本就是用的原生Fegin. 随着netflix的停更替换成了Open feign 。

```java
    /**
     * 修改契约配置，支持Feign原生的注解
     *  @return
     */
    @Bean
    public Contract feignContract() {
        return new Contract.Default();
    }
```

**注意：修改契约配置后，OrderFeignService 不再支持springmvc的注解，需要使用Feign原 生的注解 。**

```java
@FeignClient(name = "stock-service", path = "/stock", configuration = FeignConfig.class)
public interface StockFeignService {
    //声明需要调用的rest接口对应的方法，直接复制过来，对应的就行(不用写实现类)
//    @RequestMapping("/reduct")
//    String reduct();

    @RequestLine("GET/reduct")  // @RequestLine替换@RequestMapping加上请求方式GET
    String reduct();   
}
```

**3）补充，也可以通过yml配置契约**

```yaml
  #feign日志局部配置
feign:
  client:
    config:
      stock-service:
        loggerLevel: FULL
        contract: feign.Contract.Default  #设置为默认的契约（还原成原生注解）
```

### 3.3 超时时间配置 

通过 Options 可以配置连接超时时间和读取超时时间，Options 的第一个参数是连接的超时 时间（ms），默认值是 2s；第二个是请求处理的超时时间（ms），默认值是 5s。 

```java
 /*设置feign调用超时时间*/
 @Bean
 public Request.Options options() {
     return new Request.Options(5000, 10000);
 }
```

**yml中配置**

```yaml
feign:
  client:
    config:
      stock-service:
        loggerLevel: FULL
        contract: feign.Contract.Default  #设置为默认的契约（还原成原生注解）
        connectTimeout: 5000
        readTimeout: 3000
```

补充说明： Feign的底层用的是Ribbon，但超时时间以Feign配置为准

### 3.4自定义拦截器实现认证逻辑

在消费端调用提供端的时候起作用（要验证认证Authorization的时候把feign日志级别设置为FULL）

```java
public class FeignAuthRequestInterceptor implements RequestInterceptor {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void apply(RequestTemplate template) {
        logger.info("feign拦截器----");
// 业务逻辑
        String access_token = UUID.randomUUID().toString();
        template.header("Authorization", access_token);
    }
}

```

```java
//@Configuration
public class FeignConfig {
    /**
     * 日志级别
     * <p>
     *  @return
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
    /*定义拦截器*/
    @Bean
    public FeignAuthRequestInterceptor feignAuthRequestInterceptor() {
        return new FeignAuthRequestInterceptor();
    }
}
```

