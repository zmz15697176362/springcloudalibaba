#         SpringCloudAlibaba组件介绍详解（一）---nacos

**本文是关于SpringCloudAlibaba中的各种组件的详细介绍和使用，在此基础上需要已完成基本微服务框架的搭建，可以参考我的上一篇博客教你手把手搭建**：[搭建基础SpringCloudAlibaba框架]([最新微服务框架SpringCloud Alibaba介绍，搭建_厌世小晨宇yu.的博客-CSDN博客](https://blog.csdn.net/qq_45925197/article/details/126275678?spm=1001.2014.3001.5502))

本项目代码与笔记已存放在Gitee仓库   地址： [代码，笔记](https://gitee.com/zmz15697176362/springcloudalibaba)

> 以下文章是经过本人亲自一步步搭建一步步踩坑所整理的笔记，下面也对之前的版本进行了更新，看官请耐心观看~
>

[TOC]



## 1.什么是Nacos

官方：一个更易于构建云原生应用的动态**服务发现（Nacos Discovery），服务配置(Nacos Config)**和服务管理平台。

集 注册中心+配置中心+服务管理平台

Nacos 的相关特性包括：

- 服务发现和服务健康监测
- 动态配置服务
- 动态DNS服务
- 服务及元数据管理

## 2.Nacos注册中心

### 2.1 注册中心演变及其设计思想

![image.png](https://s2.loli.net/2022/08/14/p2U7DsaiOmFGNc5.png)

**存在问题：订单服务员ip：port变更，服务迁移订单服务集群部署，更改远程服务地址非常麻烦**



![image.png](https://s2.loli.net/2022/08/14/CArahZEsyvlU1ST.png)

**存在问题：订单服务水平扩容的问题，某个订单服务宕机**

![image.png](https://s2.loli.net/2022/08/14/MVvuzjU8Ri5ZqKb.png)

**存在问题：成百上千的服务nginx配置文件会变得非常复杂，运维人员内心是崩溃的。。。**



**4. 于是, 就有了注册中心的概念. 最初, 我们的想法也很简单**

![](https://img2022.cnblogs.com/blog/811915/202202/811915-20220218145054512-1361598141.png)

首先有一个数据库表来维护所有的服务, 并标记这些服务的启动状态

然后, 每当有一个服务启动, 那么都调用注册接口, 其实注册接口就是一个insert服务器信息到数据库的过程

第三, 每次商品服务要调用订单服务了, 先去数据库里面查询可用的订单服务列表. 然后根据策略选择服务ip, 

第四, 根据ip发送请求. 





这里面也会有一些问题

- 服务宕机了怎么办? 还来不及发出通知
- 每次商品服务调用订单服务, 都要去数据库查询可用的服务列表, 这样当流量大了, 就会给数据库造成很大的压力, 而且, 每次都查数据库, 效率也不高. 
- 注册中心宕机 了怎么办?

于是, 想到将我们的注册中心进行改造. 改造的更加完美一些

![](https://img2022.cnblogs.com/blog/811915/202202/811915-20220218145132482-417620616.png)

1.  这个就是在上面的基础上改造过来的
2.  增加了一个last_heartTime, 记录心跳时间.
3.  **当商品服务和订单服务启动的时候, 需要调用注册接口**, 告诉注册中心, 我上线了, 实质上这是一个insert记录的过程
4. 商品服务和订单服务有一个定时任务timetask1, 定期发送心跳. 然后注册中心就会修改这个心跳时间. 通常是30秒发送一次. 
5. 商品服务有一个定时任务timerTask2, 定期去任务中心拉取服务列表, 并将其保存在客户端缓存中, 当请求过来的时候, 通过ribbon拉取客户端缓存的ip, 按照负载均衡策略, 选择指定的订单服务发送远程调用, 
6.  在注册中心有一个定时任务timerTask3,  如果注册中心在规定的时间内, 没有收到微服务的心跳, 那么就认为服务挂了, 将其状态设置为down, 下次拉取的时候, 这台服务器不会被拉取过去. 其实,这是一个状态修改的过程
7.  **当服务停止的时候, 会调用服务注销接口**, 通知注册中心,服务停止, 注册中心就是将其从注册表中删除. 其实这就是一个delete记录的过程.

### 2.2 核心功能

**服务注册：**Nacos Server Cilent 会通过发送REST请求的方式向Nacos Server 注册自己的服务，提供自身的元数据，比如IP地址，端口信息等。Nacos Server 接收到注册请求后，就会把这些元数据信息存储在一个双层的内存Map中。

**服务心跳**：在服务注册后，Nacos Client 会维护一个定时心跳来持续通知Nacos Server ,说明服务一直处于可用状态，防止被剔除。默认5秒发送一次心跳。

**服务同步：**Nacos Server 集群之间会互相同步服务实例，用来保证服务信息的一致性。Leader raft

**服务发现:**服务消费者（Nacos Client）在调用服务提供者的服务时，会发送一个REST请求给Nacos Server ,获取上面的服务清单，并且缓存在Nacos Client本地，同时会在Nacos Client本地开启一个定时任务拉取服务端最新的注册表信息更新到本地缓存。

**服务健康检查：**Nacos Server 会开启一个定时任务用来检查注册服务实例的健康状况，对于超过5秒没有收到客户端心跳的实例会将它的healthy属性设置为false（客户端服务发现时不会发现），如果是某个实例超过30秒没有收到心跳，直接剔除该实例（被剔除的实例如果恢复发送心跳则会重新注册）





**主流的注册中心**

**CAP特性**：  （C一致性  A可用性  P 分区容错性）  任何一个注册中心都无法同时满足这三个特性。

| 核心功能        | Nacos                      | Eureka      | Consul            | CoreDNS    | Zookeeper  |
| --------------- | -------------------------- | ----------- | ----------------- | ---------- | ---------- |
| 一致性协议      | CP+AP                      | AP          | CP                |            | CP         |
| 健康检查        | TCP/HTTP/MYSQL/Client Beat | Client Beat | TCP/HTTP/gRPC/Cmd |            | Keep Alive |
| 负载均衡策略    | 权重/metadata/Selector     | Ribbon      | Fabio             | RoundRobin |            |
| 雪崩保护        | 有                         | 有          | 无                | 无         | 无         |
| 自动注销实例    | 支持                       | 支持        | 支持              | 不支持     | 支持       |
| 访问协议        | HTTP/DNS                   | HTTP        | HTTP/DNS          | DNS        | TCP        |
| 监听支持        | 支持                       | 支持        | 支持              | 不支持     | 支持       |
| 多数据中心      | 支持                       | 支持        | 支持              | 不支持     | 不支持     |
| 跨注册中心同步  | 支持                       | 不支持      | 支持              | 不支持     | 不支持     |
| SpringCloud集成 | 支持                       | 支持        | 支持              | 不支持     | 支持       |
| Dubbo集成       | 支持                       | 不支持      | 支持              | 不支持     | 支持       |
| K8S集成         | 支持                       | 不支持      | 支持              | 支持       | 不支持     |

## 3.Nacos Server 部署

#### 下载[源码](https://so.csdn.net/so/search?q=源码&spm=1001.2101.3001.7020)编译

源码下载地址：[下载地址  ](https://github.com/alibaba/nacos/)   可以用迅雷下载

![image.png](https://s2.loli.net/2022/08/16/7xUI3PWbOQefwZK.png)

点击Release 

![image.png](https://s2.loli.net/2022/08/16/2a4U78HTzrOF3cN.png)

由于我们前面搭建的SpringCloudAlibaba的基础版本是2.2.5 RELEASE 对应其组件版本如下

![image.png](https://s2.loli.net/2022/08/16/v2wkMW7UVsoPIn8.png)

所以我们需要下载nacos 1.4.1

![image.png](https://s2.loli.net/2022/08/16/qeiUtvXFpzBJcSE.png)

往下拉，点击Asserts展开

![image.png](https://s2.loli.net/2022/08/16/6kptFzDwTsmEiuG.png)

这里面的.tar.gz是Linux的安装包  , .zip是windows的安装包

由于方便讲解测试，**本文主要讲解windows版本**  两个其实差异不大

#### 下载nacos安装包

##### **windows 版本**

下载地址：[下载地址](https://github.com/alibaba/nacos/releases)

安装包已存于我的网盘，需要的也可以自取。

链接：[nacos-server-1.4.1.zip](https://pan.baidu.com/s/1iq3L5OeNS8AR6RawkULLTA )
提取码：v3bx



下载到本地，进行解压

[![vgRikR.png](https://s1.ax1x.com/2022/08/25/vgRikR.png)](https://imgse.com/i/vgRikR)

> windows版本点击这个startup.cmd即可启动（但是得对配置进行修改，因为它的默认启动方式是集群的模式）



[![vgRnne.png](https://s1.ax1x.com/2022/08/25/vgRnne.png)](https://imgse.com/i/vgRnne)



#### 修改启动模式

> 修改启动方式为单机模式



- 点击右键进行编辑 ，把 set MODE="cluster"  改成  set MODE="standalone"  (单机模式)

[![vWZB01.png](https://s1.ax1x.com/2022/08/28/vWZB01.png)](https://imgse.com/i/vWZB01)

#### 启动nacos

> ps:     nacos的默认端口是8848

双击startup.cmd即可以单机模式启动



[![vgW9v8.png](https://s1.ax1x.com/2022/08/25/vgW9v8.png)](https://imgse.com/i/vgW9v8)



#### 测试访问nosos

- 通过启动后的链接访问nacos

- 默认用户名和密码都是nacos

[![vgWn2V.png](https://s1.ax1x.com/2022/08/25/vgWn2V.png)](https://imgse.com/i/vgWn2V)

[![vgWN26.png](https://s1.ax1x.com/2022/08/25/vgWN26.png)](https://imgse.com/i/vgWN26)

[![vgWUxK.png](https://s1.ax1x.com/2022/08/25/vgWUxK.png)](https://imgse.com/i/vgWUxK)

后续就可以结合我们的微服务框架进行使用

### 3.1 单机模式与集群模式

> 目前，我们一直使用的是Nacos的单例模式。在日常的个人使用和开发测试环境中，其实使用Nacos的单例模式是没有问题的。但如果项目上了生产环境的话，为了保证高可用性，以及保险起见的话，最好还是用Nacos的集群模式。
>

具体可以参考下面这篇文章

[单例与集群搭建](https://blog.csdn.net/I_am_fine_/article/details/124440973?ops_request_misc=&request_id=&biz_id=102&utm_term=nacos%E5%8D%95%E4%BE%8B%E4%B8%8E%E9%9B%86%E7%BE%A4&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-124440973.142^v42^pc_rank_34_2,185^v2^control&spm=1018.2226.3001.4187)

或者官方文档：

[集群官方文档：](https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html)

> 本文主要讲解介绍单机模式在微服务中的使用。下面也简单介绍集群搭建。

## 4.微服务中使用nacos

### 4.1 Nacos Client 搭建

#### 4.1.1复制两份服务

为了方便区分，把order服务和stock服务分别复制一份（Ctrl -C -V ）

[![vgWXsU.png](https://s1.ax1x.com/2022/08/25/vgWXsU.png)](https://imgse.com/i/vgWXsU)

#### 4.1.2 添加依赖

打开POM文件分别修改<artifactId>order-nacos</artifactId>为我们对应的服务名

![vgfuFA.png](https://s1.ax1x.com/2022/08/25/vgfuFA.png)

[![vgh90g.png](https://s1.ax1x.com/2022/08/25/vgh90g.png)](https://imgse.com/i/vgh90g)

再到父POM中手动添加两个刚创建的module

[![vghEpq.png](https://s1.ax1x.com/2022/08/25/vghEpq.png)](https://imgse.com/i/vghEpq)

重新刷新引入Maven

[![vghr9I.png](https://s1.ax1x.com/2022/08/25/vghr9I.png)](https://imgse.com/i/vghr9I)

然后分别给两个子POM添加nacos的依赖(这里不用添加版本号因为已经默认从父项目继承了)

```java
        <!--nacos-服务注册于发现-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
```

[![vghqDU.png](https://s1.ax1x.com/2022/08/25/vghqDU.png)](https://imgse.com/i/vghqDU)

[![vg4EUH.png](https://s1.ax1x.com/2022/08/25/vg4EUH.png)](https://imgse.com/i/vg4EUH)

#### 4.1.3 修改yml配置

> 为了在注册中心能对服务注册与发现至少得配置服务名称，IP，端口  修改两个服务的yml配置



```java
server:
  port: 8020
  #应用名称  （nacos会将该名称当作是我们的服务名称）
spring:
  application:
    name: order-service
  cloud:
    nacos:
      server-addr: 127.0.0.1:8848                #nacos的服务地址不配置的话也会给个本地8848地址
      discovery:
        username: nacos                 #配置登录到nacos登录到管理平台的用户名称和密码 不配置的话默认是nacos
        password: nacos
        namespace: public         #nacos管理平台的服务列表的命名空间名 （命名空间可以隔离不同的服务实例）
```

```java
server:
  port: 8021
  #应用名称  （nacos会将该名称当作是我们的服务名称）
spring:
  application:
    name: stock-service
  cloud:
    nacos:
      server-addr: 127.0.0.1:8848                #nacos的服务地址不配置的话也会给个本地8848地址
      discovery:
        username: nacos                 #配置登录到nacos登录到管理平台的用户名称和密码 不配置的话默认是nacos
        password: nacos
        namespace: public         #nacos管理平台的服务列表的命名空间名 （命名空间可以隔离不同的服务实例）
```

##### 配置示例

> **为了方便理解配置，下面给个例子**

![vgqG34.png](https://s1.ax1x.com/2022/08/25/vgqG34.png)

> **nacos管理平台**

![vgjVI0.png](https://s1.ax1x.com/2022/08/25/vgjVI0.png)

[![vgjMM4.png](https://s1.ax1x.com/2022/08/25/vgjMM4.png)](https://imgse.com/i/vgjMM4)

#### 4.1.4 添加客户端启动注解

这个注解不加也可以，因为在有的后续的版本默认会自动添加（这里我们先注释掉，不加）

[![v2eS76.png](https://s1.ax1x.com/2022/08/25/v2eS76.png)](https://imgse.com/i/v2eS76)

#### 4.1.5 服务启动

##### 细节报错

由于之前没有注意springboot对用nacos的版本 我用的springboot版本是2.7.2  。。。。启动order-nacos,stock-nacos两个服务的时候会报错：

[![v2mZ24.png](https://s1.ax1x.com/2022/08/25/v2mZ24.png)](https://imgse.com/i/v2mZ24)

到网上找到nacos对应的springboot的版本

[![v2msJS.png](https://s1.ax1x.com/2022/08/25/v2msJS.png)](https://imgse.com/i/v2msJS)

在父POM里面把springboot版本统一改成 2.3.2.RELEASE版本后就可以正常启动了，控制台会显示服务已注册



[![v2mUsA.png](https://s1.ax1x.com/2022/08/25/v2mUsA.png)](https://imgse.com/i/v2mUsA)

这时就会看到我们启动的两个服务被注册进服务列表了

[![v2myRg.png](https://s1.ax1x.com/2022/08/25/v2myRg.png)](https://imgse.com/i/v2myRg)

##### 服务心跳问题

此时我把order-nacos服务停掉，等了大概15秒该服务的健康状态就变成了false ，30秒左右该服务就从服务列表被剔除了（注意这个位置如果是以断点的方式启动的服务，服务停掉后并没触发心跳保护，而是直接被剔除了）

[![v2nr01.png](https://s1.ax1x.com/2022/08/25/v2nr01.png)](https://imgse.com/i/v2nr01)

#### 4.1.6版本更新 （需要更新版本的可以采纳）

> 考虑到使用得版本有点旧了，我准备再次对cloud，boot,cloudalibaba进行版本更新

具体最新的各框架，组件版本可以参照这里：    [各框架，组件版本依赖关系](https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)

> 话不多说我直接采用最近刚出的版本来踩坑，以下是我配的版本（**有一点要注意Alibababa版本到了2.2.7RElEASE版本之上后服务端也至少得使用2.x以上的版本**）

下面是服务端nacos-server-2.1.1版本 ，已存于网盘需要的自取，注意解压后路径不要带中文！！！

链接：https://pan.baidu.com/s/1_HrlZkEk0sHBhAL7gHoqtw 
提取码：2zu1

[![vWiSzR.png](https://s1.ax1x.com/2022/08/27/vWiSzR.png)](https://imgse.com/i/vWiSzR)

##### 4.1.6.1 修改POM

首先修改父POM的依赖版本

修改后如下：

```java
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <modules>
        <module>order</module>
        <module>order-nacos</module>
        <module>stock</module>
        <module>stock-nacos</module>
    </modules>
    <groupId>com.zmz.springcloud</groupId>
    <artifactId>springcloudalibaba</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>springcloudalibaba</name>
    <description>Springcloud Alibaba</description>
    <packaging>pom</packaging>

    <properties>
        <java.version>1.8</java.version>
        <spring.cloud.alibaba.version>2.2.7.RELEASE</spring.cloud.alibaba.version>
        <spring.boot.version>2.3.12.RELEASE</spring.boot.version>
        <spring.cloud.version>Hoxton.SR12</spring.cloud.version>
    </properties>

    <dependencies>
        <!--SpringBoot最基本的版本场景启动器-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
        <!--SpringBoot测试的场景启动器-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <!--SpringCloudAlibaba的版本管理 通过dependency的方式完成继承-->
        <dependencies>
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring.cloud.alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

         <!--SpringBoot的版本管理器-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-parent</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!--SpringCloud的版本管理器-->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!--打包的插件-->
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>

```

在分别修改两个子module的版本依赖如下：

**oeder-nacos的依赖**

```java
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>springcloudalibaba</artifactId>
        <groupId>com.zmz.springcloud</groupId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>order-nacos</artifactId>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!--nacos-服务注册于发现-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
    </dependencies>

</project>
```

**stock-nacos的依赖**

```java
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>springcloudalibaba</artifactId>
        <groupId>com.zmz.springcloud</groupId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>stock-nacos</artifactId>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!--nacos-服务注册于发现-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
    </dependencies>

</project>
```

##### 4.1.6.2 报错

> java.lang.IllegalArgumentException: no server available 此时会有这个报错 注册的服务找不到

于是我觉得应该是随着版本更新yml的配置方式也随之改变了，于是分别修改两个服务的yml配置如下：

**order-nacos的配置：**

```java
server:
  port: 8020
  #应用名称  （nacos会将该名称当作是我们的服务名称）
spring:
  application:
    name: order-service               #nacos的服务地址不配置的话也会给个本地8848地址

  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
        username: nacos              #配置登录到nacos登录到管理平台的用户名称和密码 不配置的话默认是nacos
        password: nacos
        namespace: public            #nacos管理平台的服务列表的命名空间名 （命名空间可以隔离不同的服务实例）

```

**stock-nacos的配置**

```java
server:
  port: 8021
  #应用名称  （nacos会将该名称当作是我们的服务名称）
spring:
  application:
    name: stock-service
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
        username: nacos              #配置登录到nacos登录到管理平台的用户名称和密码 不配置的话默认是nacos
        password: nacos
        namespace: public            #nacos管理平台的服务列表的命名空间名 （命名空间可以隔离不同的服务实例）
```

果然，在我的一系列操作下版本顺利得到了更新。。。。。

### 4.2 远程服务调用

修改order服务的getForObject里面的ip端口为stock服务的服务名来调用

[![v2uaUP.png](https://s1.ax1x.com/2022/08/25/v2uaUP.png)](https://imgse.com/i/v2uaUP)

[![v2uDgg.png](https://s1.ax1x.com/2022/08/25/v2uDgg.png)](https://imgse.com/i/v2uDgg)

然后重启服务进行调用，老规矩还是报错了。。。

[![v2u6Ds.png](https://s1.ax1x.com/2022/08/25/v2u6Ds.png)](https://imgse.com/i/v2u6Ds)

**原因在于我们没有添加负载均衡器**，在RestTemplate的构造器上加上**@LoadBalanced**注解就好了，加完后就相当于RestTemplate有了负载均衡的能力。

[![v2u52F.png](https://s1.ax1x.com/2022/08/25/v2u52F.png)](https://imgse.com/i/v2u52F)

ok,调用成功

[![vWZsk6.png](https://s1.ax1x.com/2022/08/28/vWZsk6.png)](https://imgse.com/i/vWZsk6)

#### 4.2.1轮询机制

> 订单在调用库存服务的时候采用的是轮询的调用机制，这时就需要两个不同的库存服务来进行演示：

- 点击edit configuration![v21SYt.png](https://s1.ax1x.com/2022/08/25/v21SYt.png)

-  再点击复制按钮，复制一个应用

![v21Nfx.png](https://s1.ax1x.com/2022/08/25/v21Nfx.png)

然后修改应用名 ，添加一个参数，点击应用，这样就可以复制一个应用了，我们复制两个应用设置端口8022，8023

[![v216AA.png](https://s1.ax1x.com/2022/08/25/v216AA.png)](https://imgse.com/i/v216AA)

此时发现nacos的管理平台的订单服务有三个实例，这就是负载均衡了

[![v21HNn.png](https://s1.ax1x.com/2022/08/25/v21HNn.png)](https://imgse.com/i/v21HNn)

为了方便页面显示是否轮询调用，在页面回显的时候家伙加上端口，然后再把每个服务重启。

[![v21ght.png](https://s1.ax1x.com/2022/08/25/v21ght.png)](https://imgse.com/i/v21ght)

此时就会发现端口打印在8021,8022,8023,8021,8023.....随机调用切换

[![v21R9P.png](https://s1.ax1x.com/2022/08/25/v21R9P.png)](https://imgse.com/i/v21R9P)

[![v21W1f.png](https://s1.ax1x.com/2022/08/25/v21W1f.png)](https://imgse.com/i/v21W1f)

[![v21fc8.png](https://s1.ax1x.com/2022/08/25/v21fc8.png)](https://imgse.com/i/v21fc8)

> **以上就是服务的Ribbon负载均衡轮询调用机制**

### 4.3 管理平台介绍

#### 4.3.1 命名空间

管理平台的命名空间可以根据项目来隔离分离服务，使不同的项目公用一个nacos服务端但在不同的命名空间。

[![vWFU4e.png](https://s1.ax1x.com/2022/08/27/vWFU4e.png)](https://imgse.com/i/vWFU4e)

[![vWFf3j.png](https://s1.ax1x.com/2022/08/27/vWFf3j.png)](https://imgse.com/i/vWFf3j)

修改配置把默认在public命名空间下的服务管理到SpringCloudAlibaba下面，把yml的配置改为管理平台命名空间的环境值（不是命名空间名）

[![vWkkPe.png](https://s1.ax1x.com/2022/08/27/vWkkPe.png)](https://imgse.com/i/vWkkPe)

改掉订单服务和库存服务的yml

[![vWkA8H.png](https://s1.ax1x.com/2022/08/27/vWkA8H.png)](https://imgse.com/i/vWkA8H)

刷新界面就会看到之前pubic命名空间下的服务被配置到了我们新增的命名空间

[![vWknqP.png](https://s1.ax1x.com/2022/08/27/vWknqP.png)](https://imgse.com/i/vWknqP)

#### 4.3.2 服务详情-保护阈值

雪崩保护：

- ​    保护阈值：设置0~1之间的一个值  比如设置成0.6

- ​    这里有健康实例，不健康实例，总实例数三个概念，比如这里有一个订单实例（健康状态），两个库存实例（不健康状态）

-    健康实例数/总实例数 =1/3<0.6  

- 当服务健康实例数/总实例数 < 保护阈值 的时候，说明健康实例真的不多了，这个时候保护阈值会被触
  发（状态true）

  nacos将会把该服务所有的实例信息（健康的+不健康的）全部提供给消费者，消费者可能访问到不健康
  的实例，请求失败，但这样也⽐造成雪崩要好，牺牲了⼀些请求，保证了整个系统的⼀个可⽤

- ​    临时实例yml配置：spring.cloud.nacos.discovery.ephemeral =false

> 可以在yml里把服务的实例设置成永久的实例，即时宕机了也不会被剔除

[![vWZgpD.png](https://s1.ax1x.com/2022/08/28/vWZgpD.png)](https://imgse.com/i/vWZgpD)

[![vWkNq0.png](https://s1.ax1x.com/2022/08/27/vWkNq0.png)](https://imgse.com/i/vWkNq0)

[![vWkbLt.png](https://s1.ax1x.com/2022/08/27/vWkbLt.png)](https://imgse.com/i/vWkbLt)

> 比如现在把这个库存服务直接停掉，它的健康状态会变成false,等30秒服务也不会被剔除

[![vWkvFS.png](https://s1.ax1x.com/2022/08/27/vWkvFS.png)](https://imgse.com/i/vWkvFS)

权重：

结合负载均衡器的配置，设置的权重越大，服务被分配的流量也会越大。

下线：如果服务下线后就不会再使用了。

#### 4.3.3 订阅者列表

订阅者列表可以根据服务名称查询服务被访问的记录，如下

[![vWEw34.png](https://s1.ax1x.com/2022/08/28/vWEw34.png)](https://imgse.com/i/vWEw34)



## 结语

> 好了nacos的单机模式详细介绍使用就到这里，nacos的集群以及后续的SpringCloudAlibaba的各种组件会在我的专栏SpringCloudAlibaba持续更新~

