#                  删除nacos永久服务实例

话不多说直接开始。删除服务之前得先删除实例，但是永久实例不能直接删除，得调用官方的openApi接口。

以下是官网接口链接。可供参考： [nacos官网openApi](https://nacos.io/zh-cn/docs/open-api.html)

> 首先这里有一个在命名空间SpringCloudAlibaba下一个服务，我圈出来的就是**命名空间id**

[![vWHS6H.png](https://s1.ax1x.com/2022/08/28/vWHS6H.png)](https://imgse.com/i/vWHS6H)

> 点击详情就会发现它有一个永久实例，永久实例不可直接删除

[![vWTNPU.png](https://s1.ax1x.com/2022/08/28/vWTNPU.png)](https://imgse.com/i/vWTNPU)

由于是windows版，所以我直接用postman调官方的openApi接口，如果是linux版本可以参考官网的调用方式

```java
curl -X DELETE '127.0.0.1:8848/nacos/v1/ns/instance?serviceName=nacos.test.1&ip=1.1.1.1&port=8888&clusterName=TEST1'
```

这是linux的删除方式，该命令在任意路径下执行便可。



> 在讲重点，windows下删除永久实例，使用postman

如下便是接口地址和调用的参数：

执行便可

[![vWHP0I.png](https://s1.ax1x.com/2022/08/28/vWHP0I.png)](https://imgse.com/i/vWHP0I)](https://imgse.com/i/vWTdxJ)

[![vWTgPO.png](https://s1.ax1x.com/2022/08/28/vWTgPO.png)](https://imgse.com/i/vWTgPO)

以上实例便被删除了，服务被心跳保护机制自动剔除了。

