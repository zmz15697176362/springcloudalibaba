#                      Nacos集群模式

## 1.集群模式

官方文档： [nacos集群搭建官方文档](https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html)

集群部署结构图：

![nacos集群部署结构图](https://nacos.io/img/deployDnsVipMode.jpg)

> SLB：负载均衡，通常使用nginx这个中间层来分别分发到我们的nacos服务器，分别给不同的服务器部署nacos。

## 2. 预备环境准备

> 请确保是在环境中安装使用:  各种安装方式可以查看我的其他博客的详细讲解

1. 64 bit OS Linux/Unix/Mac，推荐使用Linux系统。  [linux基础环境搭建](https://blog.csdn.net/qq_45925197/article/details/126614334?spm=1001.2014.3001.5502)
2. 64 bit JDK 1.8+； [linux安装jdk的两种方式](https://blog.csdn.net/qq_45925197/article/details/126633073?spm=1001.2014.3001.5501)
3. Maven 3.3.x+；    [linux使用wget安装Maven](https://blog.csdn.net/qq_45925197/article/details/126639343)
4. nginx作为负载均衡   [linux使用mwget安装配置nginx](https://blog.csdn.net/qq_45925197/article/details/126646501?spm=1001.2014.3001.5501)
5. mysql  [docker 常用命令以及安装mysql](https://blog.csdn.net/qq_45925197/article/details/126754030?spm=1001.2014.3001.5501)
6. 3个或3个以上Nacos节点才能构成集群。

## 3.Nacos Server  部署

### 3.1 安装nacos

下载源码编译：[https://github.com/alibaba/Nacos/releases]()  可以用迅雷下载

这里直接在linux里使用wget下载

- 先进入/usr/local

```java
cd /usr/local
```

- 创建目录

```java
mkdir nacos
```

- 进入目录

```java
cd nacos
```

- 下载nacos

```java
wget https://github.com/alibaba/nacos/releases/download/1.4.1/nacos-server-1.4.1.tar.gz
```

- 解压

```java
tar -zxvf nacos-server-1.4.1.tar.gz
```

- 重命名文件夹nacos 因为要在一台虚拟机里搭建三个nacos伪集群。所以要把上面的下载解压重命名进行三次

分别重命名为nacos8849,nacos8850,nacos8851

```java
mv nacos nacos8849
...
...
```

[![xiL8eI.png](https://s1.ax1x.com/2022/09/21/xiL8eI.png)](https://imgse.com/i/xiL8eI)

- 解压完三个nacos后删除压缩包

```java
rm -rf nacos-server-1.4.1.tar.gz
```

### 3.2 修改application.properties的配置

- 分别配置nacos的/conf/application.properties的配置，使用外置数据源(配置数据源为mysql,因为它的默认的数据源是内嵌的数据源，默认存在内存中的，三个进程各有各的进程，到时候服务注册会注册到各自的nacos服务器中，为了保证数据一致性，这里使用mysql数据源，需下载配置mysql5.7+)

  > 这里以修改8849为例子，后面再修改其他两个nacos服务配置

  ```java
  cd nacos8849/conf
  vim application.properties
```
  
  - 修改对应的端口比如8849
  - 取消数据源注释·，修改数据源为mysql
- 数据的连接数量打开
  
  ```java
  server.port=8849
    
  spring.datasource.platform=mysql
  
  db.num=1
  ##注意改成自己的mysql用户名密码
  db.url.0=jdbc:mysql://127.0.0.1:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReConnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
  db.usr.0=root
  db.password.0=123456
  ```

[![xiXUMQ.png](https://s1.ax1x.com/2022/09/21/xiXUMQ.png)](https://imgse.com/i/xiXUMQ)

### 3.3 创建mysql的nacos数据库

在windows版本的nacos/conf下有一个mysql的脚本 用navicat连接上我们的linux的mysql执行脚本创建数据库

[![xiXxot.png](https://s1.ax1x.com/2022/09/21/xiXxot.png)](https://imgse.com/i/xiXxot)

[![xijNY6.png](https://s1.ax1x.com/2022/09/21/xijNY6.png)](https://imgse.com/i/xijNY6)](https://imgse.com/i/xij8m9)](https://imgse.com/i/xijiQg)](https://imgse.com/i/xij9W8)

### 3.4添加节点

- 将conf/cluster.conf.example改为cluster.conf 。添加节点配置

- ```java
  cp cluster.conf.example cluster.conf 
  ```

```java
#编辑 cluster.conf 
vim  cluster.conf 
#ip:port 
192.168.116.100:8849
192.168.116.100:8850
192.168.116.100:8851
```

[![xijT7n.png](https://s1.ax1x.com/2022/09/21/xijT7n.png)](https://imgse.com/i/xijT7n)

### 3.5 修改bin\startup.sh）

- 如果出现内存不足：修改脚本（bin\startup.sh）的jvm参数

- ```java
  cd /usr/local/nacos/bin/nacos8849/bin
  vim startup.sh
  ```

```java
JAVA_OPT="${JAVA_OPT} -server -Xms512m -Xms512m -Xms256 -XX:MaxMetaspaceSize=128m"
```

### 3.6 启动nacos

```java
cd /usr/local/nacos/nacos8849/bin
./startup.sh
```

[![xFdQIO.png](https://s1.ax1x.com/2022/09/22/xFdQIO.png)](https://imgse.com/i/xFdQIO)](https://imgse.com/i/xivMNt)

- 访问nacos8849  如果重启虚拟机后访问被拒绝可能是服务没启动，后面可以设置开机自启

[![xiv6u4.png](https://s1.ax1x.com/2022/09/21/xiv6u4.png)](https://imgse.com/i/xiv6u4)

[![xFvSZd.png](https://s1.ax1x.com/2022/09/22/xFvSZd.png)](https://imgse.com/i/xFvSZd)

### 3.7 设置nacos开机自启

```java
#查看自己的javahome
vim /etc/profile
#我的如下
export JAVA_HOME=/usr/local/jdk1.8/jdk1.8.0_333
```

```java
# 编辑开机自启脚本
vim /lib/systemd/system/nacos.service
# 添加以下内容  注意自己的javahome和nacos的服务路径
[Unit]
Description=nacos
After=network.target

[Service]
Type=forking
Environment="JAVA_HOME=/usr/local/jdk1.8/jdk1.8.0_333"
# 启动后面可以加上 -m standalone/cluster 表示以单机/集群方式启动，默认为集群
ExecStart=/usr/local/nacos/nacos8849/bin/startup.sh
ExecReload=/usr/local/nacos/nacos8849/bin/shutdown.sh
ExecStop=/usr/local/nacos/nacos8849/bin/shutdown.sh
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

- 配置完成后启动服务，执行以下命令

```java
#重新加载服务
systemctl daemon-reload
#启用服务
systemctl enable nacos.service
#启动服务
systemctl start nacos.service
#重启虚拟机
reboot
```

### 3.8 复制8849服务的配置文件到8850，8851

```java
#进入包含三个安装包的目录
cd /usr/local/nacos

#复制application.properties 到8850
cp ./nacos8849/conf/application.properties ./nacos8850/conf/application.properties 
cp：是否覆盖"./nacos8850/conf/application.properties"？ y

#复制application.properties 到8851
cp ./nacos8849/conf/application.properties ./nacos8851/conf/application.properties 
cp：是否覆盖"./nacos8850/conf/application.properties"？ y

#复制集群配置文件到8850
cp ./nacos8849/conf/cluster.conf  ./nacos8850/conf/cluster.conf 

#复制集群配置文件到8851
cp ./nacos8849/conf/cluster.conf  ./nacos8851/conf/cluster.conf 
```

### 3.9 编辑application.properties 文件

```java
vim nacos8850/conf/application.properties 
#修改端口为8850
vim nacos8851/conf/application.properties 
#修改端口为8851
```

- 同理设置这两个服务的开机自启

> nacos8850开机自启

```java
# 编辑开机自启脚本
vim /lib/systemd/system/nacos8850.service
# 添加以下内容  注意自己的javahome和nacos的服务路径
[Unit]
Description=nacos
After=network.target

[Service]
Type=forking
Environment="JAVA_HOME=/usr/local/jdk1.8/jdk1.8.0_333"
# 启动后面可以加上 -m standalone/cluster 表示以单机/集群方式启动，默认为集群
ExecStart=/usr/local/nacos/nacos8850/bin/startup.sh
ExecReload=/usr/local/nacos/nacos8850/bin/shutdown.sh
ExecStop=/usr/local/nacos/nacos8850/bin/shutdown.sh
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

- 配置完成后启动服务，执行以下命令

```java
#重新加载服务
systemctl daemon-reload
#启用服务
systemctl enable nacos8850.service
#启动服务
systemctl start nacos8850.service
```

> 8851开机自启

```java
# 编辑开机自启脚本
vim /lib/systemd/system/nacos8851.service
# 添加以下内容  注意自己的javahome和nacos的服务路径
[Unit]
Description=nacos
After=network.target

[Service]
Type=forking
Environment="JAVA_HOME=/usr/local/jdk1.8/jdk1.8.0_333"
# 启动后面可以加上 -m standalone/cluster 表示以单机/集群方式启动，默认为集群
ExecStart=/usr/local/nacos/nacos8851/bin/startup.sh
ExecReload=/usr/local/nacos/nacos8851/bin/shutdown.sh
ExecStop=/usr/local/nacos/nacos8851/bin/shutdown.sh
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

- 配置完成后启动服务，执行以下命令

```java
#重新加载服务
systemctl daemon-reload
#启用服务
systemctl enable nacos8851.service
#启动服务
systemctl start nacos8851.service
#重启虚拟机
reboot
```

### 3.10 登录nacos管理平台查看节点信息

[![xFzZUs.png](https://s1.ax1x.com/2022/09/22/xFzZUs.png)](https://imgse.com/i/xFzZUs)

4.修改idea里面SpringcloudAlibaba的nacos配置