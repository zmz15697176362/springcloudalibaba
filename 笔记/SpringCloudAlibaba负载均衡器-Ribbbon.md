##                  SpringCloudAlibaba负载均衡器-Ribbon

[TOC]

本项目代码与笔记已存放在Gitee仓库   地址： [代码，笔记](https://gitee.com/zmz15697176362/springcloudalibaba)

## 1.什么是Ribbon

目前主流的负载均衡方案分为以下两种：

- 集中式负载均衡，在消费者和服务提供方中间使用独立的代理方式进行负载，有硬件的（比如F5），也有软件的（比如Nginx）
- 客户端根据自己的请求情况做负载均衡，Ribbon就属于客户端自己做负载均衡。

SpringCloud Ribbon是基于NetFlix Ribbon实现的一套**客户端的负载均衡工具**，Ribbon客户端组件提供一些列的完善的配置，如超时，重试等。通过**Load Balancer**获取到服务器提供的所有机器实例，Ribbon会自动基于某种规则（轮询，随机）去调用这些服务，Ribbon也可以实现我们自己的负载均衡算法。

### 1.1 客户端的负载均衡

例如spring cloud 中的Ribbon，客户端有一个服务地址列表，在发送请求前通过负载均衡算法去选择一个服务器，然后进行访问，这时客户端负载均衡，即在客户端就进行负载均衡算法分配。

### 1.2服务端的负载均衡

例如Nginx, 通过Nginx进行负载均衡，先发送请求，然后通过负载均衡算法，在多个服务器之间选择一个进行访问；即可在服务端再进行负载均衡算法分配。

### 1.3 常见的负载均衡算法

- 随机，通过随机选择服务进行执行，一般这种方式使用较少。
- 轮询，负载均衡默认实现方式，请求来之后排队处理。
- 加权轮询，通过服务器的性能分型，给高配置，低负载的服务器分配更高的权重，均衡各个服务器的压力。
- 地址Hash,通过客户端请求地址的HASH值取模映射进行服务器的调度。 ip hash
- 最小链接数，即时请求负载均衡了，压力不一定会均衡，最小链接数算法就是根据服务器的情况，比如请求积压数等参数，将请求分配到当前压力最小到的服务器上，最小活跃数。

## 2.Nacos使用Ribbon

1）nacos-discovery依赖了ribbon,可以不用再引入ribbon的依赖。

[![ppP3UxS.png](https://s1.ax1x.com/2023/02/28/ppP3UxS.png)](https://imgse.com/i/ppP3UxS)

2）添加@LoadBalanced注解

```java
@Configuration
public class RestConfig{
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return  new RestTemplate();
    }
}
```

3)修改Controller

```java
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/add")
    public String add() {
        System.out.println("下单成功");
        //在RestTemplate配置的时候加上了@LoadBalance启动负载均衡后就必须使用服务名进行调用了 如果使用ip端口调用则会报错
//        String msg = restTemplate.getForObject("http://192.168.13.1:8021/stock/reduct",String.class);
        String msg = restTemplate.getForObject("http://stock-service/stock/reduct", String.class);
        return "Hello word!" + msg;
    }
}
```

## 3.Ribbon负载均衡策略

### 3.1Rule接口             

![img](https://img-blog.csdnimg.cn/20210312160600721.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L20wXzQ2NDA1NTg5,size_16,color_FFFFFF,t_70)

**IRule** 

这是所有负载均衡策略的父接口，里边的核心方法就是choose方法，用来选择一个服务实例。 

IRule接口,Riboon使用该接口,根据特定算法从所有服务中,选择一个服务,IRule接口**有7个实现类**,每个实现类代表一个[负载均衡](https://so.csdn.net/so/search?q=负载均衡&spm=1001.2101.3001.7020)算法，**默认使用轮询**

**AbstractLoadBalancerRule** 

AbstractLoadBalancerRule是一个抽象类，里边主要定义了一个ILoadBalancer，这里定义它的目的主要是辅助负责均衡策略选取合适的服务端实 例。 

**1.RandomRule** 

看名字就知道，这种负载均衡策略就是**随机选择一个服务实例**，看源码我们知道，在RandomRule的无参构造方法中初始化了一个Random对象， 然后在它重写的choose方法又调用了choose(ILoadBalancer lb, Object key)这个重载的choose方法，在这个重载的choose方法中，每次利用 random对象生成一个不大于服务实例总数的随机数，并将该数作为下标所以获取一个服务实例。 

**2.RoundRobinRule** 

RoundRobinRule这种负载均衡策略叫做线性**轮询负载均衡策略**。这个类的choose(ILoadBalancer lb, Object key)函数整体逻辑是这样的：开启 一个计数器count，在while循环中遍历服务清单，获取清单之前先通过incrementAndGetModulo方法获取一个下标，这个下标是一个不断自增长 的数先加1然后和服务清单总数取模之后获取到的（所以这个下标从来不会越界），拿着下标再去服务清单列表中取服务，每次循环计数器都会加 

1，如果连续10次都没有取到服务，则会报一个警告No available alive servers after 10 tries from load balancer: XXXX。 

**3.RetryRule**（在轮询的基础上进行重试） 

看名字就知道这种负载均衡策略带有**重试**功能。首先RetryRule中又定义了一个subRule，它的实现类是RoundRobinRule，然后在RetryRule的 choose(ILoadBalancer lb, Object key)方法中，每次还是采用RoundRobinRule中的choose规则来选择一个服务实例，如果选到的实例正常就返 回，如果选择的服务实例为null或者已经失效，则在失效时间deadline之前不断的进行重试（重试时获取服务的策略还是RoundRobinRule中定义的 策略），如果超过了deadline还是没取到则会返回一个null。 

**4.WeightedResponseTimeRule（权重 —nacos的NacosRule ，Nacos还扩展了一个自己的基于配置的权重扩展）**

WeightedResponseTimeRule是RoundRobinRule的一个子类，在WeightedResponseTimeRule中对RoundRobinRule的功能进行了扩展， WeightedResponseTimeRule中会根据每一个实例的运行情况来给计算出该实例的一个**权重**，然后在挑选实例的时候则根据权重进行挑选，这样能 够实现更优的实例调用。WeightedResponseTimeRule中有一个名叫DynamicServerWeightTask的定时任务，默认情况下每隔30秒会计算一次 各个服务实例的权重，权重的计算规则也很简单，**如果一个服务的平均响应时间越短则权重越大，那么该服务实例被选中执行任务的概率也就越大**。 

**5.BestAvailableRule** 

BestAvailableRule继承自ClientConfigEnabledRoundRobinRule，它在ClientConfigEnabledRoundRobinRule的基础上主要增加了根据 loadBalancerStats中保存的服务实例的状态信息来**过滤掉失效的服务实例的功能，然后顺便找出并发请求最小的服务实例来使用。**然而 loadBalancerStats有可能为null，如果loadBalancerStats为null，则BestAvailableRule将采用它的父类即 ClientConfigEnabledRoundRobinRule的服务选取策略（线性轮询）。 

**6.ZoneAvoidanceRule** （**默认规则**，复合判断server所在区域的性能和server的可用性选择服务器。） 

ZoneAvoidanceRule是PredicateBasedRule的一个实现类，只不过这里多一个过滤条件，ZoneAvoidanceRule中的过滤条件是以 ZoneAvoidancePredicate为主过滤条件和以 AvailabilityPredicate为次过滤条件组成的一个叫做CompositePredicate的组合过滤条件，过滤成功之后，继续采用线性轮询 (**RoundRobinRule**)的方式从过滤结果中选择一个出来。 

**7.AvailabilityFilteringRule**（先过滤掉故障实例，再选择并发较小的实例）  过滤掉一直连接失败的被标记为circuit tripped的后端Server，并过滤掉那些高并发的后端Server或者使用一个AvailabilityPredicate来 包含过滤server的逻辑，其实就是检查status里记录的各个Server的运行状态。 

### 3.2 修改默认负载均衡策略

全局配置：调用其他微服务，一律使用指定的负载均衡算法

```java
@Configuration
public class RibbonRandomRuleConfig {
    @Bean
    public IRule iRule() {
        return new RandomRule();
    }
}
```

**注意：此处有坑。**不能写在@SpringbootApplication注解的@CompentScan扫描得到的地方，否则自定义的配置类就会被所有的  RibbonClients共享。 不建议这么使用，推荐yml方式

利用@RibbonClient指定微服务及其负载均衡策略。

```java
@SpringBootApplication
@EnableDiscoveryClient   //启动nacos的客户端 不加也行在后续的版本这个注解可不用手动添加
@RibbonClients(value = {
        // 在SpringBoot主程序扫描的包外定义配置类
        @RibbonClient(name = "stock-service", configuration = RibbonRandomRuleConfig.class)
     
})
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    @Bean
    @LoadBalanced   //负载均衡注解，加上后就必须使用服务名调用
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        RestTemplate restTemplate = builder.build();
        return restTemplate;
    }


}
```

**使用配置文件方式**：调用指定微服务提供的服务时，使用对应的负载均衡算法 （得先把上面的@RibbonClients注释掉，不然会冲突）注意配置的上下级关系

修改application.yml 

```yaml
1 # 被调用的微服务名
2 mall‐order:
3  ribbon:
4  # 指定使用Nacos提供的负载均衡策略（优先调用同一集群的实例，基于随机&权重）
5    NFLoadBalancerRuleClassName: com.alibaba.cloud.nacos.ribbon.NacosRule
```

### 3.3 自定义负载均衡策略 

通过实现 IRule 接口可以自定义负载策略，主要的选择服务逻辑在 choose 方法中。 

修改启动类

```java
public class CustomRule extends AbstractLoadBalancerRule {
    private static final Logger logger = Logger.getLogger("CustomRule.class");
    @Autowired
    private NacosDiscoveryProperties nacosDiscoveryProperties;

    @Override
    public Server choose(Object key) {
        DynamicServerListLoadBalancer loadBalancer = (DynamicServerListLoadBalancer) getLoadBalancer();
        String serviceName = loadBalancer.getName();
        NamingService namingService = nacosDiscoveryProperties.namingServiceInstance();
        try {
            //nacos基于权重的算法
            Instance instance = namingService.selectOneHealthyInstance(serviceName);
            return new NacosServer(instance);
        } catch (NacosException e) {
            logger.info("获取服务实例异常：{}" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void initWithNiwsConfig(IClientConfig clientConfig) {

    }
}

```

**修改配置类**

```java
@SpringBootApplication
@EnableDiscoveryClient   //启动nacos的客户端 不加也行在后续的版本这个注解可不用手动添加
@RibbonClients(value = {
        // 在SpringBoot主程序扫描的包外定义配置类
//        @RibbonClient(name = "stock-service", configuration =  RibbonRandomRuleConfig.class)
        @RibbonClient(name = "stock-service", configuration = CustomRule.class)
})
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    @Bean
    @LoadBalanced   //负载均衡注解，加上后就必须使用服务名调用
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        RestTemplate restTemplate = builder.build();
        return restTemplate;
    }

}
```

### 3.4 饥饿加载

在进行服务调用的时候，如果网络情况不好，第一次调用会超时。 

Ribbon默认懒加载，意味着只有在发起调用的时候才会创建客户端。

开启饥饿加载，解决第一次调用慢的问题

```javascript
ribbon:
  eager-load:
    enabled: true # 开启饥饿加载
    clients:  # 指定饥饿加载的服务名称
      - stock-service
```

### 4.LoadBalancer

**什么是Spring Cloud LoadBalancer?**

Spring Cloud LoadBalancer是Spring Cloud官方自己提供的客户端负载均衡器, 用来替代 

Ribbon。

Spring官方提供了两种负载均衡的客户端： 

**RestTemplate** 

RestTemplate是Spring提供的用于访问Rest服务的客户端，RestTemplate提供了多种便捷访问 

远程Http服务的方法，能够大大提高客户端的编写效率。默认情况下，RestTemplate默认依赖 

jdk的HTTP连接工具。 

**WebClient** 

WebClient是从Spring WebFlux 5.0版本开始提供的一个非阻塞的基于响应式编程的进行Http请 

求的客户端工具。它的响应式编程的基于Reactor的。WebClient中提供了标准Http请求方式对 

应的get、post、put、delete等方法，可以用来发起相应的请求。 

**2. RestTemplate整合LoadBalancer** 

**1）引入依赖** 

```java
 <!‐‐ LoadBalancer ‐‐>
<dependency>
<groupId>org.springframework.cloud</groupId>
<artifactId>spring‐cloud‐starter‐loadbalancer</artifactId>
</dependency>

<!‐‐ 提供了RestTemplate支持 ‐‐>
<dependency>
<groupId>org.springframework.boot</groupId>
<artifactId>spring‐boot‐starter‐web</artifactId>
</dependency>

<!‐‐ nacos服务注册与发现 移除ribbon支持‐‐>
<dependency>
<groupId>com.alibaba.cloud</groupId>
<artifactId>spring‐cloud‐starter‐alibaba‐nacos‐discovery</artifactId>
<exclusions>
<exclusion>
<groupId>org.springframework.cloud</groupId>
<artifactId>spring‐cloud‐starter‐netflix‐ribbon</artifactId>
</exclusion></exclusions>
</dependency>
```

> 注意： nacos-discovery中引入了ribbon，需要移除ribbon的包 
>
> 如果不移除，也可以在yml中配置不使用ribbon

```yaml
spring:
  application:
  name: order-service
  cloud:
  nacos:
  discovery: 
  server‐addr: 127.0.0.1:8848
  # 不使用ribbon
    loadbalancer:
    ribbon:
    enabled: false
```

**2）使用@LoadBalanced注解配置RestTemplate**

```java
 @Configuration
 public class RestConfig {
  @Bean
  @LoadBalanced
 public RestTemplate restTemplate() {
 return new RestTemplate();
 }
 }
```

 **3)使用**

```java
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/add")
    public String add() {
        System.out.println("下单成功");
        //在RestTemplate配置的时候加上了@LoadBalance启动负载均衡后就必须使用服务名进行调用了 如果使用ip端口调用则会报错
//        String msg = restTemplate.getForObject("http://192.168.13.1:8021/stock/reduct",String.class);
        String msg = restTemplate.getForObject("http://stock-service/stock/reduct", String.class);
        return "Hello word!" + msg;
    }
}
```

