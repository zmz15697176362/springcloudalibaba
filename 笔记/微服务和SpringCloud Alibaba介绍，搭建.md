# 微服务和SpringCloud Alibaba详细介绍（一），手把手搭建微服务框架

PS：本博客是本人参照B站博主：**JAVA阿伟如是说** 的视频讲解手敲整理的笔记  跟着一起手动搭建的框架  供大家一起学习   ~    希望各位看官也能自己亲自动手搭建 ~~

[视频链接： Springcloud详细教学]([[保姆级入门教程]我翻遍整个B站这绝对是讲的最好的SpringCloud Alibaba全套教程（没有之一），从入门到项目实战，小白必备。_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1uN4y1j7gs?p=1&vd_source=d76c8714efd01e91cb503fef6c102f6b))

本项目代码与笔记已存放在Gitee仓库   地址： [代码，笔记](https://gitee.com/zmz15697176362/springcloudalibaba)

## 1.微服务介绍

### 1.1系统架构演变

随着互联网的发展，网站的应用规模也在不断的扩大，进而导致系统架构也在不断的进行变化。从互联网到现在，系统的架构大体经历了下面几个过程，**单体应用架构（ORM）->垂直应用架构（MVC）->分布式架构（RPC）>SOA架构（ESB/Dubbo）->微服务架构(SpringCloud)**，当然还有悄然兴起的Service Mesh(服务网格化).接下来我们就来了解一下每种系统架构是什么样子的，以及各有什么优缺点。

#### 1.1.1 单体应用架构（ORM）

互联网早期，一般的网站应用流量较小，只需一个应用，将所有的功能代码都部署在一起就可以了，这杨可以减少开发，部署和维护的成本。

比如说一个电商系统，里面会包含很多用户管理，商品管理，订单管理，物流管理等很多模块，我们会把它们做成一个Web项目，然后部署到**一台Tomcat服务器**上。

**优点：**

- 项目架构简单，小型项目的话，开发成本低。
- 项目部署在一个节点上，维护方便

**缺点：**

- 全部部署在一个工程中，对于大型项目来讲不易开发和维护
- 项目模块之间紧密耦合，单点容错率低
- 无法针对不同的模块进行针对性优化和水平扩展

#### 1.1.2 垂直应用架构（MVC）

随着访问量的逐渐增大，单一应用只能依靠增加节点来应对，但是这时候会发现不是所有的模块都会有比较大的访问量。

还是以上面的电商为例子，用户访问量的增多可能影响的只是用户和订单模块，但是对消息模块的影响就比较小，那么此时我们希望只多增加几个模块，而不增加消息模块，此时的单体应用就做不到了，垂直应用就应运而生了。所谓的垂直应用架构，就是将原来的应用拆成互不相干的几个应用，以提升效率，比如我们可以将上面电商的单体应用拆分成：

- 电商系统（用户管理 商品管理 订单管理）
- 后台系统（用户管理 订单管理 客户管理）
- CMS系统（广告管理 营销管理）

这样分析完毕之后，一旦用户访问量大，只需要增加电商系统的节点就可以了，而无需增加后台和CMS的节点。**（对访问量大的模块进行负载均衡就可以了）**

**优点：**

- 系统拆分实现了流量分担，解决并发问题，而且可以针对不同模块进行优化和水平扩展。
- 一个系统的问题不会影响到其他系统，提高容错率。

**缺点：**

- 系统之间相互独立无法进行相互调用。

- 系统之间独立，会有重复的开发任务。

  

#### 1.1.3 分布式架构(RPC)

当垂直应用越来越多，重复的业务代码就会会越来越多，这时候，我们就思考可不可以将重复的代码抽取出来，做成统一的业务层，作为独立的服务，然后由前端控制层调用不同的业务层服务呢？这就产生了新的分布式系统架构。它将**把工程拆分成表现层和服务层两个部分**，服务层中包含业务逻辑。表现层只需要处理和页面的交互，业务逻辑都是调用服务层的服务来实现。

**优点：**

- **抽取公共的功能为服务层**，提高代码复用性。

**缺点：**

- 系统之间耦合度变高，调用关系错综复杂，难以维护。

#### 1.1.4 SOA架构（ESB/Dubbo）

在分布式架构下，当服务越来越多，容量的评估，小服务资源的浪费等问题逐渐显现，此时需增加一个调度中心对集群进行实时管理。此时，用于资源调度和治理中心（SOA Service Oriented Architecture）是关键。

**优点：**

- **使用治理中心**（ESB/Dubbo）解决服务间**调用关系的自动调节。**

**缺点：**

- 服务间会有依赖关系，一旦其环节出错会影响较大（服务雪崩）
- 服务关系复杂，运维，测试部署困难。

#### 1.1.5 微服务架构（SpringCloud）

微服务架构在某种程度上是面向服务的架构SOA继续发展的下一步，它更加强调服务的“彻底拆分”。**微服务架构也是一个分布式架构。**

##### 微服务架构与SOA架构的不同

微服务架构比SOA架构粒度会更加精细，让专业的人去做专业的事情（专注），目的提高效率，每个服务与服务之间互不影响，微服务架构中，**每个服务必须独立部署**，微服务架构更加轻巧，轻量级。

SOA架构中可能数据库存储会发生共享，微服务强调**每个服务都单独数据库**，保证每个服务间互不影响。

项目体现特征微服务比SOA架构更加适合于互联网公司**敏捷开发，快速迭代版本，因为粒度非常精细**。

**优点：**

- 服务原子化拆分，独立打包，部署和升级，保证每个微服务清晰的任务划分，利于扩展。
- 微服务之间采用RestFul等轻量级http协议互相调用。

**缺点：**

- 分布式系统开发的技术成本高（容错，分布式事务等）
- 复杂性也高，各个微服务进行分布式独立部署，当进行模块调用的时候，分布式将会变得更加麻烦。



### 1.2 常见的微服务架构

#####     1.dubbo：zookeeper+dubbo +SpringMVC+SpringBoot

- 配套 通信方式： rpo

- 注册中心: zookeeper/redis

- 配置中心：diamond   

  ##### 2.SpringCloud: 全家桶+轻松嵌入第三方组件（Netflix）

-  配套 通信方式 ：http restful

- 注册中心： eruka /consul

- 配置中心 :config

- 断路器：hystrix

- 网关： zuul

- 分布式追踪系统： sleuth+zipkin  

  ##### 3 .SpringCloud alibaba

  分布式系统中的常见模式给了Spring Cloud 一个清晰的定位，即“模式”。也就是说Springcloud 是针对分布式系统开发做的通用对象，是标准的实现。这个定义非常抽象，看完之后并不能知道Springcloud具体包含什么内容，再来看一下SpringCloud官方给的一个High Light 的架构图就可以对这种模式有更清晰的认识。·

## 2.SpringCloud Alibaba环境版本

SpringCloud Alibaba依赖Java环境来运行，还需要为此配置Maven环境，请确保是在以下版本环境中安装使用：

​	1.64bit JDK 1.8+ ;下载&配置 1.8.0_131

​	2.Maven 3.2x+;下载&配置。3.6.1

**1.基于SpringBoot 的父Maven项目**

**2.创建2个服务 （订单服务和库存服务）**

**组件版本关系**

| SpringCloud Alibaba Version                      | Sentinel Version | Nacos Vsersion | RocketMQ Version | Dubbo Version | Seata Version |
| ------------------------------------------------ | ---------------- | -------------- | ---------------- | ------------- | ------------- |
| 2.2.4 RELEASE                                    | 1.8.0            | 1.4.1          | 4.4.0            | 2.7.8         | 1.3.0         |
| 2.2.3 RELEASE or 2.1.3 RELEASE or 2.0.3 RELEASE  | 1.8.0            | 1.3.3          | 4.4.0            | 2.7.8         | 1.3.0         |
| 2.2.1 RELEASE or 2.1.2 RELEASE or 2.0.2 RELEASE  | 1.7.1            | 1.2.1          | 4.4.0            | 2.7.6         | 1.2.0         |
| 2.2.0 RELEASE                                    | 1.7.1            | 1.1.4          | 4.4.0            | 2.7.4.1       | 1.0.0         |
| 2.2.1  RELEASE or 2.0.1 RELEASE or 1.5.1 RELEASE | 1.7.0            | 1.1.4          | 4.4.0            | 2.7.3         | 0.9.0         |
| 2.1.0 RELEASE or 2.0.0 RELEASE or 1.5.0 RELEASE  | 1.6.3            | 1.1.1          | 4.4.0            | 2.7.3         | 0.7.1         |

## 3 .手把手搭建分布式项目

1.首先创建一个spring Initializar 父项目

![1.png](https://s2.loli.net/2022/08/09/K23BgaPDtLWr8sC.png)

2. 注意此处Type选择为Maven POM

![2.png](https://s2.loli.net/2022/08/09/rfvn2htc5ijQloV.png)

3.NEXT

![3.png](https://s2.loli.net/2022/08/09/CIMB3QOg426VXGJ.png)

4.修改路径->finish

![4.png](https://s2.loli.net/2022/08/09/bCh3z6wfFX9Utuy.png)

5.修改父级项目的打包方式为pom![5.png](https://s2.loli.net/2022/08/09/xdH1ioEWLU5CkYe.png)

6.创建两个服务Module

![image.png](https://s2.loli.net/2022/08/09/U13ajvknSIZqM9D.png)

7.服务创建推荐选择Maven方式创建（因为此处子项目可以自动继承父项目的POM）![image.png](https://s2.loli.net/2022/08/09/LgV2WFrAY8KUQHv.png)

8.NEXT  .先创建一个订单服务order

![image.png](https://s2.loli.net/2022/08/09/9mXbJRao6tVgleW.png)

9.再创建一个库存服务stock

![image.png](https://s2.loli.net/2022/08/09/VICzqudnKkAeSap.png)

总体结构如图

![image.png](https://s2.loli.net/2022/08/09/KWdZyEvX5pH3bjA.png)

10.在**两个**子服务里加上两个SpringMVC的Web启动器![image.png](https://s2.loli.net/2022/08/10/rp9zvXn7CyHklTu.png)

11.添加完依赖后写上我们对应的**两个**Controller

![image.png](https://s2.loli.net/2022/08/09/wBXsm348qaJ2ecO.png)

![image.png](https://s2.loli.net/2022/08/09/Rpvb62ihAuQeYmg.png)

12.再配置两个启动类





![image.png](https://s2.loli.net/2022/08/09/PiWx45Kg6bT83zs.png)

![image.png](https://s2.loli.net/2022/08/09/i23rlzJ8O7DwUef.png)

### 3.1.分布式架构--使用基本的RestTemplate 进行远程调用

欧克  现在开始使用订单服务调用库存服务 记住因为两个子服务并没相互依赖 所以并不能像一般springboot项目的模块一样模块之间直接调用

为了简便  因为启动类也是个配置类 在启动类里面配置RestTemplate （实际开发不推荐这么些） 通过RestTemplateBuilder 构造器构建 

![image.png](https://s2.loli.net/2022/08/09/9YSc1eGPw63i87a.png)

**ps:  如果这个builder报错，可以去掉勾选Autowiring for bean calss**

![image.png](https://s2.loli.net/2022/08/09/YOewkuKgJMBsjh3.png)

ps:  如果启动报错Error:(4, 47) java: 程序包org.springframework.web.bind.annotation不存在

![image.png](https://s2.loli.net/2022/08/09/XWztGfY38H4ejRb.png)

先给订单服务和库存服务分别配置端口为8010和8011

![image.png](https://s2.loli.net/2022/08/09/arVvcH3WTpEeCki.png)

![image.png](https://s2.loli.net/2022/08/09/wBpsPmuQfjclbzY.png)

然后在OrderController 使用远程调用StockController的接口  第一个参数是库存服务的ip端口地址 ，第二个参数是调用的接口的返回值类型

![image.png](https://s2.loli.net/2022/08/09/Tfdqcv29mbWDB1t.png) 

再启动**两个服务**

使用postMan测试  控制台输出了下单成功  返回值是Hello word!扣减库存！

![image.png](https://s2.loli.net/2022/08/09/EBxkFzAbLUVrmp3.png)

![image.png](https://s2.loli.net/2022/08/09/g8iCIjJ6DZRBnmq.png)

**总结：以上的分布式架构相比于微服务架构未使用注册中心 ，不方便服务的治理和维护！！！RPC远程调用也不太方便。**





## 4.SpringCloudAlibaba搭建

SpringCloud Alibaba 依赖JAVA环境来运行。还需要为此配置Maven环境，请确保是在以下环境版本中安装使用：

1.  64 bit   JDK 1.8+  ；下载&配置。1.8.0_131
2. Maven  3.2 x+  ;下载&配置。   3.6.1

1.基于SpringBoot的父Maven项目

2.创建2个服务（订单服务和库存服务）



**组件版本关系**

| Spring Cloud Alibaba Version                               | Sentinel Version | Nacos Version | RocketMQ | Dubbo Version | Seata  Version |
| ---------------------------------------------------------- | ---------------- | ------------- | -------- | ------------- | -------------- |
| 2021.1 OR  2.2.5.RELEASE OR 2.1.4 RELEASE OR 2.0.4.RELEASE | 1.8.0            | 1.4.1         | 4.4.0    | 2.7.8         | 1.3.0          |
| RELEASE OR 2.1.3 RELEASE OR 2.0.3 RELEASE                  | 1.8.0            | 1.3.3         | 4.4.0    | 2.7.8         | 1.3.0          |
| 2.2.1 RELEASE OR 2.1.2 RELEASE OR 2.0.2 RELEASE            | 1.7.1            | 1.2.1         | 4.4.0    | 2.7.6         | 1.2.0          |
| 2.2.0 RELEASE                                              | 1.7.1            | 1.1.4         | 4.4.0    | 2.7.4.1       | 1.0.0          |
| 2.1.1 RELEASE OR 2.0.1 RELEASE OR 1.5.1 RELEASE            | 1.7.0            | 1.1.4         | 4.4.0    | 2.7.3         | 0.9.0          |
| 2.1.0 RELEASE OR  2.0.1 RELEASE OR 1.5.0 RELEASE           | 1.6.3            | 1.1.1         | 4.4.0    | 2.7.3         | 0.7.1          |

可以去官网查看具体i详细版本说明

 本次搭建使用 SpringCloud Alibaba 2.2.5.RELEASE  ； 

​                         Spring Boot 2.3.2.RELEASE；

​                         Spring Cloud   Hoxton.SR8

 （ 加入了SpringCloudAlibaba的版本后就不用再关心上面的微服务组件的版本了）

**毕业版本依赖关系（推荐使用）**

| Spring Cloud Version        | SpringCloud Alibaba Version       | Spring Boot Version |
| --------------------------- | --------------------------------- | ------------------- |
| Spring Cloud 2020.0.0       | 2021.1                            | 2.4.2               |
| Spring Cloud Hoxton.SR8     | 2.2.5.RELEASE                     | 2.3.2.RELEASE       |
| Spring Cloud Greenwish.SR6  | 2.1.4.RELEASE                     | 2.1.13.RELEASE      |
| Spring Cloud Hoxton.SR3     | 2.2.2.RELEASE                     | 2.2.5.RELEASE       |
| Spring Cloud Hoxton.RELEASE | 2.2.0.RELEASE                     | 2.2.X.RELEASE       |
| Spring Cloud Greenwich      | 2.1.2.RELEASE                     | 2.1.X.RELEASE       |
| Spring Cloud Finchley       | 2.0.4.RELEASE(停止维护，建议升级) | 2.0.X.RELEASE       |
| Spring Cloud Edgware        | 1.5.1.RELEASE(停止维护，建议升级) | 1.5.X.RELEASE       |



### 开始引入依赖

**在父项目POM**里面通过<dependencyManagement></dependencyManagement>方式加入SpringCloud Alibaba的版本管理 后续在子项目中加入微服务的组件时就不用再加入版本了

![image.png](https://s2.loli.net/2022/08/10/pAq7vo69VrWG8SK.png)

可以把下面的SpringBoot的版本管理器也用上面的<dependencyManagement></dependencyManagement>方式管理 因为有的公司会自行创建一个<parent></parent> 这样就方便后续管理![image.png](https://s2.loli.net/2022/08/10/6tEI9w7PVf2q3O4.png)

修改为如下

![image.png](https://s2.loli.net/2022/08/10/fDPLTjFepZ5b86J.png)

同理再加入SpringCloud的版本管理  

![image.png](https://s2.loli.net/2022/08/10/6fsYCwXztRgjBxE.png)

为了方便版本维护，可以在<properties></properties> 对版本管理器的版本进行配置

![image.png](https://s2.loli.net/2022/08/10/aDFQC7WwtGcY6Sl.png)

![image.png](https://s2.loli.net/2022/08/10/ngwrJY4ApTsX9N1.png)

最终父项目的POM依赖如下

```java
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <modules>
        <module>order</module>
        <module>stock</module>
    </modules>
    <groupId>com.zmz.springcloud</groupId>
    <artifactId>springcloudalibaba</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>springcloudalibaba</name>
    <description>Springcloud Alibaba</description>
    <packaging>pom</packaging>

    <properties>
        <java.version>1.8</java.version>
        <spring.cloud.alibaba.version>2.2.5.RELEASE</spring.cloud.alibaba.version>
        <spring.boot.version>2.7.2</spring.boot.version>
        <spring.cloud.version>Hoxton.SR8</spring.cloud.version>
    </properties>

    <dependencies>
        <!--SpringBoot最基本的版本场景启动器-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
        <!--SpringBoot测试的场景启动器-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <!--SpringCloudAlibaba的版本管理 通过dependency的方式完成继承-->
        <dependencies>
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring.cloud.alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

         <!--SpringBoot的版本管理器-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-parent</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!--SpringCloud的版本管理器-->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!--打包的插件-->
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>

```

后续就可以直接到对应的子Maven项目中添加微服务组件了

至此SpringCloudAlibaba最基本的环境搭建就完成了

## 

后续微服务组件内容会在接下来的博客专栏 **Springcloud Alibaba**专栏更新上传   ~~~

