# nacos-config配置中心

本项目代码与笔记已存放在Gitee仓库   地址： [代码，笔记](https://gitee.com/zmz15697176362/springcloudalibaba)

[TOC]

官方文档： [https://github.com/alibaba/spring­cloud­alibaba/wiki/Nacos­config](https://github.com/alibaba/spring­cloud­alibaba/wiki/Nacos­config ) 

Nacos 提供用于存储配置和其他元数据的 key/value 存储，为分布式系统中的外部化配置提供服务器端和客户端支持。使用 Spring Cloud Alibaba Nacos Config，您可以在 Nacos Server 集中管理你 Spring Cloud 应用的外部属性配置。 

1.维护性 2.时效性 3.安全性 

**springcloud config 对比**

**三大优势：**

- springcloud config大部分场景结合git 使用, 动态变更还需要依赖Spring Cloud Bus 消息总线来通过所有的客户端变化
- springcloud config不提供可视化界面
- nacos config使用长轮询更新配置, 一旦配置有变动后，通知Provider的过程非常的迅速, 从速度上秒杀springcloud原来的config几条街。

## 1.1 快速开始

点击加号新建配置![](https://pic.imgdb.cn/item/6416c3f3a682492fcca4be2a.jpg)

![](https://pic.imgdb.cn/item/6416c477a682492fcca59f8e.jpg)

> Namespace：代表不同**环境**，如开发、测试、生产环境。 
>
> Group：代表某**项目**，如XX医疗项目、XX电商项目 
>
> DataId：每个项目下往往有若干个**工程（微服务）**，每个配置集(DataId)是一个**工程（微服务）**的**主配置文件** 

![](https://pic.imgdb.cn/item/6416c797a682492fccaadf5b.jpg)

- 比如新建一个公共订单配置，点击发布，就会出现在配置列表。

![](https://pic.imgdb.cn/item/6416c6dfa682492fcca9b484.jpg)

![](https://pic.imgdb.cn/item/6416c548a682492fcca7156b.jpg)

- 还可以直接勾选配置点击克隆到其他命名空间（环境）

 **权限控制**

- 启动权限：如果需要使用权限配置，修改application.properties，使之生效

![](https://pic.imgdb.cn/item/6416c65aa682492fcca8d823.jpg)

## 1.2 搭建nacos-config服务

通过 Nacos Server 和 spring-cloud-starter-alibaba-nacos-config 实现配置的动态变更

- **先new一个module**

new->module->Next->输入名字->finish

![](https://pic.imgdb.cn/item/6416c901a682492fccadff10.jpg)

- **新建启动类**

  **如果指定了namespace：public会有一个bug控制台一直打印ClientWorker**

  ![](https://pic.imgdb.cn/item/6416ca11a682492fccb06712.jpg)

- **新建配置类**

![](https://pic.imgdb.cn/item/6416ca5fa682492fccb0f7c4.jpg) 

- **添加依赖**

```java
<!--NacosConfig依赖 -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
```

- 在启动类获取配置信息(在使用之前得先加上bootstrap.properties配置文件来配置Nacos Server地址)

```java

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2023/03/19/16:36
 * @Description: 配置中心服务启动类
 */
@SpringBootApplication
public class ConfigApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ConfigApplication.class, args);
        String orderName = applicationContext.getEnvironment().getProperty("order.name");
        String num = applicationContext.getEnvironment().getProperty("order.num");
        System.out.println("order name :" + orderName + "; num: " + num);
    }
}
```

- **加上bootstrap.yml配置文件来配置Nacos Server地址**

```java
spring:
  application:
    name: config-nacos
  cloud:
    nacos:
      server-addr: 192.168.13.1:8848
      username: nacos
      password: nacos
      config:
        namespace: public
```

> 注意如果在配置里改了权限开启为true就必须得配置nacos用户名密码

此时配置未生效控制台打印为null

```java
2023-03-19 16:55:37.480  INFO 17536 --- [           main] c.a.nacos.client.config.impl.CacheData   : [fixed-192.168.13.1_8848-public] [add-listener] ok, tenant=public, dataId=config-nacos, group=DEFAULT_GROUP, cnt=1
order name :null; num: null
```

**这是因为服务名必须得和nacos里面配置的dataId保持一致**

```java
spring:
  application:
    name: com.zmz.order
  cloud:
    nacos:
      server-addr: 192.168.13.1:8848
      username: nacos
      password: nacos
      config:
        namespace: public
```

这时配置就生效了，当然如果想服务名与dataId不一致就得手动指定dataId

```java
控制台：[add-listener] ok, tenant=public, dataId=com.zmz.order, group=DEFAULT_GROUP, cnt=1
order name : 蔡徐坤; num:  21
```

## 1.3 Config相关配置

Nacos 数据模型 Key 由三元组唯一确定, Namespace默认是空串，公共命名空间（public），分组默认是  

DEFAULT_GROUP 

![](https://pic.imgdb.cn/item/6416cf86a682492fccb95d6b.jpg)

**支持配置的动态更新**

```java
@SpringBootApplication
public class ConfigApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ConfigApplication.class, args);
        while(true){
        //当动态配置刷新时，会更新到 Enviroment中，因此这里每隔一秒中从Enviroment中获取配置
        String orderName = applicationContext.getEnvironment().getProperty("order.name");
        String num = applicationContext.getEnvironment().getProperty("order.num");
        System.out.println("order name :" + orderName + "; num: " + num);
        TimeUnit.SECONDS.sleep(1);
        }
    }
}
```

**ps：除了默认的配置文件， 其他dataId都要加上后缀**

**Nacos客户端默认是Properties类型的配置如果想使用yml需要如下配置**

```java
spring:
  application:
    name: com.zmz.order
  cloud:
    nacos:
      server-addr: 192.168.13.1:8848
      username: nacos
      password: nacos
      config:
        namespace: public
        file-extension: yaml
     #   refresh-enabled: false   客户端将无法感知配置的变化
        
#Nacos客户端默认是Properties类型的配置如果想使用yml需要 file-extension: yaml(只针对默认配置文件和profile格式配置文件，可以自定义配置文件)
```

### 1.3.1 支持profile粒度的配置

(不同的环境不同的配置文件比如application-dev.yml )

```java
server:
  port: 8050
spring:
  profiles:
    active: dev
```

spring­cloud­starter­alibaba­nacos­config 在加载配置的时候，不仅仅加载了以 dataid 命名为 ${spring.application.name}.${file-extension:properties} 为**前缀**的基础配置，还加载了dataid命名为 ${spring.application.name}-${profile}.${file-extension:properties} 的基础配置**（除了默认的配置文件，其他配置文件必须写上后缀）**。在日常开发中如果遇到多套环境下的不 同配置，可以通过Spring 提供的 ${spring.profiles.active} 这个配置项来配置。

**但是只有默认配置文件（跟服务名相同的dataId的配置文件）可以使用profile粒度的配置**

### 示例：

创建一个-dev的配置文件

> 这里会有一个配置文件生效的优先级（优先级大的会覆盖优先级小的并且互补），前提是去掉指定 file-extension: yaml配置文件后缀
>
> profile>默认配置文件>自定义配置文件

![](https://pic.imgdb.cn/item/6416d572a682492fccc23f18.jpg)

### 1.3.2 支持命名空间分类配置(按环境规类)

**支持自定义** **namespace** **的配置** 

用于进行租户粒度的配置隔离。不同的命名空间下，可以存在相同的 Group 或 Data ID 的配置。Namespace 的常用场景 之一是不同环境的配置的区分隔离，例如开发测试环境和生产环境的资源（如配置、服务）隔离等。

直接修改config.namespace为指定的**命名空间id**就可以了

```java
 spring.cloud.nacos.config.namespace=8f46651d-94f6-4697-8054-8bc25b815165
```

### 1.3.3 支持自定义 Group 的配置（按项目规类）

Group是组织配置的维度之一。通过一个有意义的字符串（如 Buy 或 Trade ）对配置集进行分组，从而区分 Data ID 相同的配置集。当您在 Nacos 上创建一个配置时，如果未填写配置分组的名称，则配置分组的名称默认采用DEFAULT_GROUP 。配置分组的常见场景：不同的应用或组件使用了相同的配置类型，如 database_url 配置和MQ_topic 配置。在没有明确指定 ${spring.cloud.nacos.config.group} 配置的情况下，默认是DEFAULT_GROUP 。如果需要自定义自己的Group，可以通过以下配置来实现：

```java
spring.cloud.nacos.config.group= GROUP_ZMZ
```

### 1.3.4  支持自定义扩展的 Data Id 配置

Data ID  是组织划分配置的维度之一。Data ID 通常用于组织划分系统的配置集。一个系统或者应用可以包含多个配置 集，每个配置集都可以被一个有意义的名称标识。Data ID 通常采用类 Java 包（如 com.taobao.tc.refund.log.level）的命 名规则保证全局唯一性。此命名规则非强制。

![](https://pic.imgdb.cn/item/6416dad2a682492fcccbe6c5.jpg)

- 修改application测试

```java
@SpringBootApplication
public class ConfigApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ConfigApplication.class, args);
        String orderName = applicationContext.getEnvironment().getProperty("order.name");
        String num = applicationContext.getEnvironment().getProperty("order.num");
        String userConfig = applicationContext.getEnvironment().getProperty("user.config");
        System.out.println("order name :" + orderName + "; num: " + num);
        System.out.println("userConfig :" + userConfig);
    }
}
```

- 修改yaml

```java
spring:
  application:
    name: com.zmz.order
  cloud:
    nacos:
      server-addr: 192.168.13.1:8848
      username: nacos
      password: nacos
      config:
        namespace: public
        #        namespace: 8f46651d-94f6-4697-8054-8bc25b815165
        file-extension: yaml
        #   refresh-enabled: false   客户端将无法感知配置的变化

        group: DEFAULT_GROUP
        shared-configs:
          - data-id: com.zdy.common.properties
            refresh: true
        extension-configs:
          - data-id: com.extension.common.properties
            refresh: true
#Nacos客户端默认是Properties类型的配置如果想使用yml需要 file-extension: yaml(只针对默认配置文件和profile格式配置文件，可以自定义配置文件)
#除了默认的配置文件，其他配置文件必须写上后缀
#profile>默认配置文件>自定义配置文件( extension-configs:>shared-configs，在下表越大优先级越大)
```

### 1.3.5 @RefreshScope

@Value注解可以获取到配置中心的值，但是无法动态感知修改后的值，需要利用@RefreshScope注解 

```java
@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {
    @Value("${user.name}")
    public String userName;

    @RequestMapping("/show")
    public String show() {
        return userName;
    }
}
```

