#                      Nacos集群模式

> 前言：（**有一点要注意SpringCloudAlibaba版本到了2.2.7RElEASE版本之上后服务端也至少得使用2.x以上的版本，这里使用nacos2.1.1版本）**

[TOC]

## 1.集群模式

官方文档： [nacos集群搭建官方文档](https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html)

集群部署结构图：

![nacos集群部署结构图](https://nacos.io/img/deployDnsVipMode.jpg)

> SLB：负载均衡，通常使用nginx这个中间层来分别分发到我们的nacos服务器，分别给不同的服务器部署nacos。

## 2. 预备环境准备

> 请确保是在环境中安装使用:  各种安装方式可以查看我的其他博客的详细讲解

1. 64 bit OS Linux/Unix/Mac，推荐使用Linux系统。  [linux基础环境搭建](https://blog.csdn.net/qq_45925197/article/details/126614334?spm=1001.2014.3001.5502)
2. 64 bit JDK 1.8+； [linux安装jdk的两种方式](https://blog.csdn.net/qq_45925197/article/details/126633073?spm=1001.2014.3001.5501)
3. Maven 3.3.x+；    [linux使用wget安装Maven](https://blog.csdn.net/qq_45925197/article/details/126639343)
4. nginx作为负载均衡   [linux使用mwget安装配置nginx](https://blog.csdn.net/qq_45925197/article/details/126646501?spm=1001.2014.3001.5501)
5. mysql  [docker 常用命令以及安装mysql](https://blog.csdn.net/qq_45925197/article/details/126754030?spm=1001.2014.3001.5501)
6. 3个或3个以上Nacos节点才能构成集群。

## 3.Nacos Server  部署

### 3.1 安装nacos

下载源码编译：[https://github.com/alibaba/Nacos/releases]()  可以用迅雷下载

这里直接在linux里使用wget下载

- 先进入/usr/local

```java
cd /usr/local
```

- 创建目录

```java
mkdir nacos-2.1.1
```

- 进入目录

```java
cd nacos-2.1.1
```

- 下载nacos

```java
wget https://github.com/alibaba/nacos/releases/download/2.1.1/nacos-server-2.1.1.tar.gz
```

- 解压

```java
tar -zxvf nacos-server-2.1.1.tar.gz
```

- 重命名文件夹nacos 因为要在一台虚拟机里搭建三个nacos伪集群。所以要把上面的下载解压重命名进行三次

分别重命名为nacos8870,nacos8872,nacos8874

> 最好不要使用连续的端口 ，不然可能出现地址已在使用报错。

```java
mv nacos nacos8870
...
...
```

[![xkTGnI.png](https://s1.ax1x.com/2022/09/23/xkTGnI.png)](https://imgse.com/i/xkTGnI)

- 解压完三个nacos后删除压缩包

```java
rm -rf nacos-server-2.1.1.tar.gz
```

### 3.2 修改application.properties的配置

- 分别配置nacos的/conf/application.properties的配置，使用外置数据源(配置数据源为mysql,因为它的默认的数据源是内嵌的数据源，默认存在内存中的，三个进程各有各的进程，到时候服务注册会注册到各自的nacos服务器中，为了保证数据一致性，这里使用mysql数据源，需下载配置mysql5.7+)

  > 这里以修改8870为例子，后面再修改其他两个nacos服务配置

  ```java
  cd nacos8870/conf
  vim application.properties
  ```
```
  
  - 修改对应的端口比如8870
  - 取消数据源注释·，修改数据源为mysql
- 数据的连接数量打开
  
  ```java
  server.port=8870
    
  spring.datasource.platform=mysql
  
  db.num=1
  ##注意改成自己的mysql用户名密码
  db.url.0=jdbc:mysql://127.0.0.1:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReConnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
  db.usr.0=root
  db.password.0=123456
```

[![xki3qK.png](https://s1.ax1x.com/2022/09/23/xki3qK.png)](https://imgse.com/i/xki3qK)

### 3.3 创建mysql的nacos数据库

在windows版本的nacos/conf下有一个mysql的脚本 用navicat连接上我们的linux的mysql执行脚本创建数据库

[![xiXxot.png](https://s1.ax1x.com/2022/09/21/xiXxot.png)](https://imgse.com/i/xiXxot)

[![xijNY6.png](https://s1.ax1x.com/2022/09/21/xijNY6.png)](https://imgse.com/i/xijNY6)

### 3.4添加节点

- 将conf/cluster.conf.example改为cluster.conf 。添加节点配置

- ```java
  cp cluster.conf.example cluster.conf 
  ```

```java
#编辑 cluster.conf 
vim  cluster.conf 
#ip:port 
192.168.116.100:8870
192.168.116.100:8872
192.168.116.100:8874
```

[![xkTDjs.png](https://s1.ax1x.com/2022/09/23/xkTDjs.png)](https://imgse.com/i/xkTDjs)

### 3.5 修改bin\startup.sh）

- 如果出现内存不足：修改脚本（bin\startup.sh）的jvm参数

- ```java
  cd /usr/local/nacos-2.1.1/nacos8870/bin
  vim startup.sh
  ```

```java
   JAVA_OPT="${JAVA_OPT} -server -Xms512m -Xmx512m -Xmn256m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=128m"
```

### 3.6 启动nacos

```java
cd /usr/local/nacos-2.1.1/nacos8870/bin
./startup.sh
```

[![xkiUGd.png](https://s1.ax1x.com/2022/09/23/xkiUGd.png)](https://imgse.com/i/xkiUGd)

- 访问nacos8870  如果重启虚拟机后访问被拒绝可能是服务没启动，后面可以设置开机自启

[![xkT6H0.png](https://s1.ax1x.com/2022/09/23/xkT6H0.png)](https://imgse.com/i/xkT6H0)

### 3.7 设置nacos开机自启

```java
#查看自己的javahome
vim /etc/profile
#我的如下
export JAVA_HOME=/usr/local/jdk1.8/jdk1.8.0_333
```

```java
# 编辑开机自启脚本
vim /lib/systemd/system/nacos8870.service
# 添加以下内容  注意自己的javahome和nacos的服务路径
[Unit]
Description=nacos
After=network.target

[Service]
Type=forking
Environment="JAVA_HOME=/usr/local/jdk1.8/jdk1.8.0_333"
# 启动后面可以加上 -m standalone/cluster 表示以单机/集群方式启动，默认为集群
ExecStart=/usr/local/nacos-2.1.1/nacos8870/bin/startup.sh
ExecReload=/usr/local/nacos-2.1.1/nacos8870/bin/shutdown.sh
ExecStop=/usr/local/nacos-2.1.1/nacos8870/bin/shutdown.sh
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

- 配置完成后启动服务，执行以下命令

```java
#重新加载服务
systemctl daemon-reload
#启用服务
systemctl enable nacos8870.service
#启动服务
systemctl start nacos8870.service
#重启虚拟机
reboot
```

### 3.8 复制8870服务的配置文件到8872，8874

```java
#进入包含三个安装包的目录
cd /usr/local/nacos-2.1.1

#复制application.properties 到8872
cp ./nacos8870/conf/application.properties ./nacos8872/conf/application.properties 
cp：是否覆盖"./nacos8872/conf/application.properties"？ y

#复制application.properties 到8874
cp ./nacos8870/conf/application.properties ./nacos8874/conf/application.properties 
cp：是否覆盖"./nacos8874/conf/application.properties"？ y

#复制集群配置文件到8872
cp ./nacos8870/conf/cluster.conf  ./nacos8872/conf/cluster.conf 

#复制集群配置文件到8874
cp ./nacos8870/conf/cluster.conf  ./nacos8874/conf/cluster.conf 
```

### 3.9 编辑application.properties 文件

```java
vim nacos8872/conf/application.properties 
#修改端口为8872
vim nacos8874/conf/application.properties 
#修改端口为8874
```

- 同理设置这两个服务的开机自启

> nacos8872开机自启

```java
# 编辑开机自启脚本
vim /lib/systemd/system/nacos8872.service
# 添加以下内容  注意自己的javahome和nacos的服务路径
[Unit]
Description=nacos
After=network.target

[Service]
Type=forking
Environment="JAVA_HOME=/usr/local/jdk1.8/jdk1.8.0_333"
# 启动后面可以加上 -m standalone/cluster 表示以单机/集群方式启动，默认为集群
ExecStart=/usr/local/nacos-2.1.1/nacos8872/bin/startup.sh
ExecReload=/usr/local/nacos-2.1.1/nacos8872/bin/shutdown.sh
ExecStop=/usr/local/nacos-2.1.1/nacos8872/bin/shutdown.sh
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

- 配置完成后启动服务，执行以下命令

```java
#重新加载服务
systemctl daemon-reload
#启用服务
systemctl enable nacos8872.service
#启动服务
systemctl start nacos8872.service
```

> 8874开机自启

```java
# 编辑开机自启脚本
vim /lib/systemd/system/nacos8874.service
# 添加以下内容  注意自己的javahome和nacos的服务路径
[Unit]
Description=nacos
After=network.target

[Service]
Type=forking
Environment="JAVA_HOME=/usr/local/jdk1.8/jdk1.8.0_333"
# 启动后面可以加上 -m standalone/cluster 表示以单机/集群方式启动，默认为集群
ExecStart=/usr/local/nacos-2.1.1/nacos8874/bin/startup.sh
ExecReload=/usr/local/nacos-2.1.1/nacos8874/bin/shutdown.sh
ExecStop=/usr/local/nacos-2.1.1/nacos8874/bin/shutdown.sh
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

- 配置完成后启动服务，执行以下命令

```java
#重新加载服务
systemctl daemon-reload
#启用服务
systemctl enable nacos8874.service
#启动服务
systemctl start nacos8874.service
#重启虚拟机
reboot
```

### 3.10 登录nacos管理平台查看节点信息

[![xkH1eI.png](https://s1.ax1x.com/2022/09/23/xkH1eI.png)](https://imgse.com/i/xkH1eI)

## 4.nginx反向代理nacos集群配置

> 下面我会介绍如何在cloudalibaba框架中使用nacos集群，前面的基础框架搭建过程以及代码在我的SpringCloudAlibaba专栏有详细介绍

- 首先我们得使用nginx负载均衡进行分发，指向我们的nacos集群

这里已经提前安装好了nginx

[![xEIern.png](https://s1.ax1x.com/2022/09/25/xEIern.png)](https://imgse.com/i/xEIern)



[![xEIaa6.png](https://s1.ax1x.com/2022/09/25/xEIaa6.png)](https://imgse.com/i/xEIaa6)

- 进入默认配置文件

```java
cd nginx/conf
```

- 打开**默认配置文件**

```java
 vim nginx.conf
```

- 修改集群负载均衡配置,官方推荐，nginx方向代理,注意修改成自己的ip端口,还有nginx配置的“；”，(这里我把我安装的nginx配置端口为8089，并设置了开机自启，不会的可以参照我的文章开头推荐的环境准备进行配置)

如果访问的是虚拟机的localhost:8089/nacos 就会负载均衡访问我们的nacos集群地址http://nacoscluster/nacos/

```java
    upstream nacoscluster {
        server 127.0.0.1:8870;
        server 127.0.0.1:8872;
        server 127.0.0.1:8874;
    }
    server {
        listen        8089;
        server_name   localhost;
        
        location /nacos/ {
            proxy_pass http://nacoscluster/nacos/;
        }
    }
```

- 修改配置，保存退出，重启虚拟机

  ```java
  reboot
  ```

[![xETcUP.png](https://s1.ax1x.com/2022/09/25/xETcUP.png)](https://imgse.com/i/xETcUP)

- 配置测试成功

[![xEHlwR.png](https://s1.ax1x.com/2022/09/25/xEHlwR.png)](https://imgse.com/i/xEHlwR)

## 5.修改idea配置

### 5.1 修改SpringCloudAlibaba配置

- 修改SpringCloudAlibaba的stock和order服务的nacos配置

[![xELpQO.png](https://s1.ax1x.com/2022/09/25/xELpQO.png)](https://imgse.com/i/xELpQO)

[![xEHyff.png](https://s1.ax1x.com/2022/09/25/xEHyff.png)](https://imgse.com/i/xEHyff)

- 启动服务这时我的控制台报错，原因是我的springCloudAlibababa版本是2.2.7RELEASE,linux的nacos客户端版本是2.1.1 我尝试2.2.7RELEASEspringCloudAlibababa版本连接windows的nacos2.1.1 并不会报错但是连接linux的此版本就会报错,后来参照网上的把springCloudAlibababa版本换成了2.1.2RELEASE就没报错了。

> 具体的版本问题我也并不知为何，只是做了简单的测试，现在采用的版本如下

[![xEqcLQ.png](https://s1.ax1x.com/2022/09/25/xEqcLQ.png)](https://imgse.com/i/xEqcLQ)

### 5.2 访问linux  nacos管理平台

[![xELkTA.png](https://s1.ax1x.com/2022/09/25/xELkTA.png)](https://imgse.com/i/xELkTA)