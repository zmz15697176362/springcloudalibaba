package com.zmz.order.config;

import com.zmz.order.interceptor.FeignAuthRequestInterceptor;
import feign.Logger;
import org.springframework.context.annotation.Bean;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2023/03/15/22:21
 * @Description:
 */
/*  全局配置：当使用@Configuation会将配置作用于所有的服务提供方
 *   局部配置: 如果只想针对某一个服务进行配置，就不要加@Configuration
 */
//@Configuration
public class FeignConfig {
    /**
     * 日志级别
     * <p>
     *  @return
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    /**
     * 修改契约配置，支持Feign原生的注解
     *  @return
     */
/*  @Bean
    public Contract feignContract() {
        return new Contract.Default();
    }*/

    /*设置feign调用超时时间*/
/*    @Bean
    public Request.Options options() {
        return new Request.Options(5000, 10000);
    }*/
    /*定义拦截器*/
    @Bean
    public FeignAuthRequestInterceptor feignAuthRequestInterceptor() {
        return new FeignAuthRequestInterceptor();
    }
}