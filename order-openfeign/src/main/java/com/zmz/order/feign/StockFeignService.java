package com.zmz.order.feign;

import com.zmz.order.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2023/03/15/21:15
 * @Description: 调用stock-service服务service
 */
/*name 指定调用的rest接口的服务名，比如这里调用库存服务stock-service
 * path 指定rest接口所在的StockController指定的@RequestMapping("/stock")
 *  **/
@FeignClient(name = "stock-service", path = "/stock", configuration = FeignConfig.class)
public interface StockFeignService {
    //声明需要调用的rest接口对应的方法，直接复制过来，对应的就行(不用写实现类)
    @RequestMapping("/reduct")
    String reduct();

/*    @RequestLine("GET/reduct")  // @RequestLine替换@RequestMapping加上请求方式GET
    String reduct();*/
}
