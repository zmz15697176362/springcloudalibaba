package com.zmz.order.controller;

import com.zmz.order.feign.StockFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create with IntelliT IDEA
 *
 * @Author: zhengmingzhe
 * @Date: 2022/08/09/20:49
 * @Description: 订单服务
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    StockFeignService stockService;

    @RequestMapping("/add")
    public String add() {
        System.out.println("下单成功");
        String msg = stockService.reduct();
        return "Hello Feign!" + msg;
    }

}
